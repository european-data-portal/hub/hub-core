# hub-core

This is the core module for the hub. It syncs the triple store and the search index and provides a rich RESTful API.

## Prerequisites

* Java JDK 1.8
* Maven

## Setup

1.  Clone repository
1.  Navigate into the cloned directory
1.  Create configuration  
    ```
    $ cp conf/config.sample.json conf/config.json 
    ```
4. Edit config.conf
    * See [settings configuration](#configuration)
1.  Start the application
    ```
    $ mvn package exec:java
    ```
1.  Browse to http://localhost:8080 for API specification

## Build and Run with Docker

build:
```bash
$ mvn clean package
$ sudo docker build -t=hub .
```

run:
```bash
$ sudo docker run -p 8080:8080 -d hub
```

## Configuration 
a sample configuration can be found in [conf/config.sample.json](conf/config.sample.json)


**NOTE**: Required fields to be configured are: `HUB_API_KEY` and `HUB_TRIPLESTORE_CONFIG`

### Variables



* `HUB_API_KEY`: the API key of the hub, needed to modify the triplestore
   
* `HUB_SERVICE_PORT`: the port on which this server will be accessed, default: 8080

* `HUB_TRIPLESTORE_CONFIG`: a JSON Object; set this to configure the backend triplestore access fields that have to be set are most probably `host`, `user` and `password`. If you have another Triplestore than virtuoso, you might also need to configure the other fields.
     ```json
        {
            "host": "<triplestore domain with protocol>",
            "port": "<triplestore port>",
            "data_endpoint": "</endpoint for modifying the triplestore>",
            "query_endpoint": "</endpoint for querying the triplestore>",
            "ping": "<ping endpoint, if the service supports this, default: empty>",
            "user": "<triplestore username>",
            "password": "<triplestore password>"
          
        }
     ```
* `HUB_VALIDATOR`: a JSON Object; set the field `url` to the validator url
     ```json
        {
          "enabled": "<boolean, default: true>",
          "url": "<validator domain with protocol and port>"
        }
     ```
* `HUB_SEARCH_SERVICE`:  a JSON Object; set the field `url` to the url of the 
     ```json
        {
           "url": "<search service domain with protocol>",
           "port": "<search service port>",
           "api_key": "<search-service-api-key>"
        }
     ```
* `HUB_TRANSLATION_SERVICE`: a JSON Object;
     ```json
        {
            "enable": "<boolean, default: true>",
            "accepted_languages": "<array of country codes of accepted languages>",
            "translation_service_url": "<translation service domain with protocol>",
            "callback_url": "<callback url to the translation module on this service>"
                }
     ```
* `"HUB_DATA_UPLOAD`: a JSON Object;
     ```json
        {
              "url": "<data access url with protocol and port>",
              "service_url": "<data upload domain with protocol and port>",
              "api_key": "<data-upload-service-api-key>"
        }
     ```
* `HUB_LOAD_VOCABULARY`: boolean, default: true
* `greeting`: greeting message for health website
* `HUB_BASE_URI`: the base uri every Graph uses


