package io.piveau.hub.verticles;

import io.piveau.hub.converters.CatalogToIndexConverter;
import io.piveau.hub.converters.DatasetToIndexConverter;
import io.piveau.hub.services.catalogues.CataloguesService;
import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.util.CatalogueHelper;
import io.piveau.hub.util.Constants;
import io.piveau.hub.util.DatasetHelper;

import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.cli.Argument;
import io.vertx.core.cli.CLI;
import io.vertx.core.cli.CommandLine;
import io.vertx.core.cli.Option;
import io.vertx.core.file.FileSystem;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.shell.ShellService;
import io.vertx.ext.shell.ShellServiceOptions;
import io.vertx.ext.shell.command.CommandBuilder;
import io.vertx.ext.shell.command.CommandProcess;
import io.vertx.ext.shell.command.CommandRegistry;
import io.vertx.ext.shell.term.HttpTermOptions;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.DCAT;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


public class ShellVerticle extends AbstractVerticle {



    @Override
    public void start(Future<Void> future) {

        JsonObject cliConfig = config().getJsonObject(Constants.ENV_HUB_SEARCH_CLI_CONFIG, new JsonObject());
        Integer cliPort = cliConfig.getInteger("port", 8085);
        String cliType = cliConfig.getString("type", "http");

        if (cliType.equals("http")) {
            ShellServiceOptions shellServiceOptions = new ShellServiceOptions()
                    .setWelcomeMessage(" Welcome to piveau-hub CLI!")
                    .setHttpOptions(new HttpTermOptions()
                            .setHost("0.0.0.0")
                            .setPort(cliPort));
            ShellService service = ShellService.create(vertx, shellServiceOptions);
            service.start(ar -> {
                if (ar.succeeded()) {
                    CommandRegistry registry = CommandRegistry.getShared(vertx);
                    registry.registerCommand(cleanUpSearch().build(vertx));
                    registry.registerCommand(reIndexCatalog().build(vertx));
                    registry.registerCommand(reIndexDataset().build(vertx));
                    registry.registerCommand(reIndexAllDatasets().build(vertx));
                    registry.registerCommand(loadDatasets().build(vertx));
                    registry.registerCommand(reIndexCatalogItself().build(vertx));
                    future.complete();
                } else {
                    future.fail(ar.cause());
                }
            });
        }
    }

    private CommandBuilder loadDatasets() {

        DatasetsService datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
        CataloguesService cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);

        ArrayList<Argument> arguments = new ArrayList<>();


        arguments.add(createArgument("catalogId", "The id of the dataset to be used for finding the dataset.", "0", true));

        CommandBuilder resetIndexes = CommandBuilder.command(createCLI("loadDatasets", arguments));
        resetIndexes.processHandler(process -> {

            process.write("Are you sure you want to clean up the search module ? [y/n]\n");
            process.stdinHandler(data -> {
                if (data.equals("y")) {

                    String catalogid = process.args().get(0);
                    int limit = 200;

                    FileSystem fileSystem = vertx.fileSystem();
                    Buffer bufferCatalog = fileSystem.readFileBlocking("doc/example_catalog.ttl");
                    cataloguesService.putCatalogue(catalogid, bufferCatalog.toString(), "text/turtle", null, handler -> {
                        if (handler.succeeded()) {
                            for (AtomicInteger i = new AtomicInteger(0); i.get() <= limit; i.getAndIncrement()) {
                                Buffer buffer = fileSystem.readFileBlocking("doc/example_dataset.ttl");
                                datasetsService.putDataset(catalogid + i.get(), buffer.toString(), "text/turtle", catalogid, null, false, subhandler -> {
                                    PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(catalogid + i.get(),catalogid, getClass());
                                    if (handler.succeeded()) {
                                        //LOGGER.info("Successfully added Dataset");
                                    } else {
                                        LOGGER.error("Problem adding the dataset");
                                    }
                                    if (i.get() == limit)
                                        process.end();
                                });
                            }
                        }
                    });
                } else {
                    process.end();
                }
            });
        });
        return resetIndexes;
    }


    /**
     * @return CommandBuilder
     * <p>
     * A new shell command in order to cleanup all existing indexes in the search hub.
     */
    private CommandBuilder cleanUpSearch() {

        IndexService indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS);
        ArrayList<Argument> arguments = new ArrayList<>();

        CommandBuilder resetIndexes = CommandBuilder.command(createCLI("cleanUpSearch", arguments));
        resetIndexes.processHandler(process -> {

            process.write("Are you sure you want to clean up the search module ? [y/n]\n");
            process.stdinHandler(data -> {
                if (data.equals("y")) {

                    process.write("Please wait we will clean the searchIndex now of all orphans... \n");

                    getAllDatasetIds(handler -> {
                        ArrayList<String> datasets = handler.result();
                        clearIndex(indexService, datasets, process, 0, clearHandler -> {
                            if (clearHandler.succeeded())
                                process.write("Successfully cleared the index \n").end();
                            else
                                process.write("Problem clearing the searchIndex" + clearHandler.cause().getMessage()).end();
                        });
                    });
                } else {
                    process.end();
                }
            });
        });
        return resetIndexes;
    }


    /**
     * @return CommandBuilder
     * <p>
     * A new shell command in order to reindex existing datasets by providing a dataset and catalog id.
     */
    private CommandBuilder reIndexDataset() {

        DatasetsService datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);

        //creating the command with two arguments

        ArrayList<Argument> arguments = new ArrayList<>();
        arguments.add(createArgument("datasetId", "The id of the dataset to be used for finding the dataset.", "dataset-1", true));
        arguments.add(createArgument("catalogId", "The id of the catalog to be used for finding the catalog.", "catalog-1", true));

        CommandBuilder resetIndexes = CommandBuilder.command(createCLI("reIndexDataset", arguments));
        resetIndexes.processHandler(process -> {

            process.write("Are you sure you want to reindex of this dataset? [y/n] \n");
            process.stdinHandler(data -> {
                if (data.equals("y")) {

                    List<JsonObject> datasets = new ArrayList<>();
                    PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(process.args().get(1),process.args().get(0),getClass());
                    datasetsService.getDataset(process.args().get(1), process.args().get(0), null, getHandler -> {
                        if (getHandler.succeeded()) {
                            JsonObject datasetWithEverything = new JsonObject();
                            datasetWithEverything.put("dataset", getHandler.result().getString("content"))
                                    .put("datasetId", process.args().get(1))
                                    .put("catalogId", process.args().get(0));
                            datasets.add(datasetWithEverything);
                            indexDatasets(datasets, false, process, indexHandler -> {
                                if (indexHandler.succeeded()) {
                                    process.write("Successfully reindexed the dataset with the datasetId : " + process.args().get(1) + "\n").end();
                                } else {

                                    LOGGER.error("There was a problem reindexing the dataset with the datasetId : " + process.args().get(1) + " Problem : " + indexHandler.cause().getMessage());
                                    process.write("There was a problem reindexing the dataset with the datasetId : " + process.args().get(1) + " Problem : " + indexHandler.cause().getMessage()).end();
                                }
                            });
                        } else
                            process.write("Problem getting the dataset" + getHandler.cause().getMessage() + "\n").end();
                    });
                } else {
                    process.end();
                }
            });
        });
        return resetIndexes;
    }

    /**
     * @return Commandbuilder
     * <p>
     * A new shell command in order to reindex existing catalog by providing catalog id.
     */
    private CommandBuilder reIndexCatalogItself() {

        CataloguesService cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);


        //creating the command with one argument
        ArrayList<Argument> arguments = new ArrayList<>();
        arguments.add(createArgument("catalogId", "The id of the catalog to be used for finding the catalog.", "catalog-1", true));

        CLI catalogCli = createCLI("reIndexCatalogId", arguments).addOption(new Option().setArgName("v").setShortName("v").setLongName("verbose").setFlag(true));

        CommandBuilder resetIndexes = CommandBuilder.command(catalogCli);
        resetIndexes.processHandler(process -> {

            CommandLine commandLine = process.commandLine();

            String catalogId = commandLine.getArgumentValue(0);

            PiveauLogger LOGGER = PiveauLoggerFactory.getCatalogueLogger(catalogId,getClass());
            //checking first if the catalog exists
            cataloguesService.getCatalogue(catalogId, "application/json", handler -> {
                if (!handler.succeeded()) {
                    process.write("There is no catalog with this id : " + catalogId + " \n").end();
                } else {
                    process.write("Are you sure you want to reindex of this catalog? This might take a while! [y/n]\n");
                    process.stdinHandler(data -> {

                        if (data.equals("y")) {
                            reindexCatalog(catalogId, handler.result().getString("content"), reindexHandler -> {
                                if (reindexHandler.succeeded())
                                    process.write("Successfully reindexed the catalog \n").end();
                                else {
                                    LOGGER.error("Problem reindexing the catalog" + reindexHandler.cause().getMessage());
                                    process.write("Problem reindexing the catalog" + reindexHandler.cause().getMessage() + " \n").end();
                                }
                            });
                        } else {
                            process.end();
                        }
                    });
                }
            });

        });
        return resetIndexes;
    }


    /**
     * @return Commandbuilder
     * <p>
     * A new shell command in order to reindex existing catalog by providing catalog id.
     */
    private CommandBuilder reIndexCatalog() {

        CataloguesService cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);

        //creating the command with one argument
        ArrayList<Argument> arguments = new ArrayList<>();
        arguments.add(createArgument("catalogId", "The id of the catalog to be used for finding the catalog.", "catalog-1", true));

        CLI catalogCli = createCLI("reIndexCatalog", arguments).addOption(new Option().setArgName("v").setShortName("v").setLongName("verbose").setFlag(true));

        CommandBuilder resetIndexes = CommandBuilder.command(catalogCli);
        resetIndexes.processHandler(process -> {

            CommandLine commandLine = process.commandLine();

            String catalogId = commandLine.getArgumentValue(0);
            PiveauLogger LOGGER = PiveauLoggerFactory.getCatalogueLogger(catalogId,getClass());
            AtomicBoolean verbose = new AtomicBoolean(commandLine.isFlagEnabled("v"));

            //checking first if the catalog exists
            cataloguesService.getCatalogue(catalogId, "application/json", handler -> {
                if (!handler.succeeded()) {
                    process.write("There is no catalog with this id : " + catalogId + " \n").end();
                } else {
                    process.write("Are you sure you want to reindex of this catalog? This might take a while! [y/n]\n");
                    process.stdinHandler(data -> {

                        if (data.equals("y")) {
                            reindexCatalog(catalogId, handler.result().getString("content"), reindexHandler -> {
                                if (reindexHandler.succeeded()) {
                                    process.write("Successfully reindexed the catalog \n");
                                    process.write("Catalog id is " + catalogId + "\n");
                                    process.write("Currently indexing the catalog, please wait... \n");
                                    listAllDatasetsAndIndex(verbose, process, catalogId, listHandler -> {
                                        if (listHandler.succeeded()) {
                                            process.write("Successfully reindexed catalog \n").end();
                                        } else {
                                            process.write("Finished reindexin the catalog with problems" + listHandler.cause().getMessage() + " \n").end();
                                        }
                                    });
                                } else {
                                    LOGGER.error("Problem reindexing the catalog it self" + reindexHandler.cause().getMessage());
                                    process.write("Problem reindexing the catalog it self" + reindexHandler.cause().getMessage() + " \n").end();
                                }
                            });
                        } else {
                            process.end();
                        }
                    });
                }
            });
        });

        return resetIndexes;
    }

    /**
     * @return Commandbuilder
     * <p>
     * A new shell command in order to reindex all existing datasets.
     */
    private CommandBuilder reIndexAllDatasets() {

        CataloguesService cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);
        ArrayList<Argument> arguments = new ArrayList<>();

        CommandBuilder resetIndexes = CommandBuilder.command(createCLI("reIndexAllDatasets", arguments));
        resetIndexes.processHandler(process -> {

            process.write("Are you sure you want to reindex of ALL dataset ? This will take a while! [y/n]\n");
            process.stdinHandler(data -> {
                if (data.equals("y")) {

                    process.write("Currently indexing all datasets, please wait... \n");

                    //TODO limit of catalogs now at 1000 pagination once we reach that limit
                    int limit = 1000;

                    cataloguesService.listCatalogues("application/json", limit, 0, catalogueHandler -> {
                        if (catalogueHandler.succeeded()) {

                            //TODO Getting the catalog id is not supported yet so this hack takes the id out of the URI which is bad!
                            JsonObject listOfCatalogs = new JsonObject(catalogueHandler.result().getString("content"));
                            JsonArray catalogueList = listOfCatalogs.getJsonObject("results").getJsonArray("bindings");

                            AtomicInteger catalogcounter = new AtomicInteger(0);
                            List<Future> catalogsFutureList = new ArrayList<>();

                            for (Object catalog : catalogueList) {

                                Future<Void> catalogFuture = Future.future();
                                catalogsFutureList.add(catalogFuture);

                                //TODO Getting the catalog id is not supported yet so this hack takes the id out of the URI which is bad!
                                JsonObject JsonCatalog = (JsonObject) catalog;
                                String catalogURI = JsonCatalog.getJsonObject("g").getString("value");
                                String catalogId = catalogURI.substring(catalogURI.indexOf("catalogue/") + 10);
                                PiveauLogger LOGGER = PiveauLoggerFactory.getCatalogueLogger(catalogId,getClass());
                                catalogcounter.incrementAndGet();

                                process.write("Currently reindexing " + catalogId + ". Overall progress : " + catalogueList.size() + " / " + catalogcounter + "\n");

                                //reindex all datasets in the current selected catalog using the indexMultDatasets function
                                listAllDatasetsAndIndex(new AtomicBoolean(false), process, catalogId, allHandler -> {
                                    if (allHandler.succeeded()) {
                                        process.write("Successfully reindexed all datasets of catalog \n");
                                        catalogFuture.complete();
                                    } else {
                                        LOGGER.error("Finished reindexing all datasets of catalog with problems" + allHandler.cause().getMessage());
                                        process.write("Finished reindexing all datasets of catalog with problems" + allHandler.cause().getMessage() + " \n");
                                        catalogFuture.complete();
                                    }
                                });
                            }
                            CompositeFuture.join(catalogsFutureList).setHandler(catalogListHandler -> {
                                if (catalogListHandler.succeeded()) {
                                    process.write("Finished reindexing all datasets \n").end();
                                } else {
                                    PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());
                                    LOGGER.error("Finished reindexing all datasets with problems" + catalogListHandler.cause().getMessage());
                                    process.write("Finished reindexing all datasets with problems" + catalogListHandler.cause().getMessage() + " \n").end();
                                }
                            });
                        }
                    });
                } else {
                    process.end();
                }
            });
        });
        return resetIndexes;
    }

    private void reindexCatalog(String catalogId, String catalog, Handler<AsyncResult<Void>> handler) {

        IndexService indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS);

        CatalogueHelper catalogueHelper = new CatalogueHelper(catalogId, "application/json", catalog);
        catalogueHelper.model(helperhandler -> {
            if (helperhandler.succeeded()) {
                CatalogToIndexConverter catalogToIndexConverter = new CatalogToIndexConverter();
                JsonObject indexMessage = catalogToIndexConverter.convert(catalogueHelper.getModel(), catalogId);
                indexService.addCatalog(indexMessage, catalogHandler -> {
                    if (catalogHandler.succeeded()) {
                        handler.handle(Future.succeededFuture());
                    } else {
                        handler.handle(Future.failedFuture("Problem reindexing the catalog" + catalogHandler.cause().getMessage()));
                    }
                });
            } else {
                handler.handle(Future.failedFuture("Problem using the CatalogHelper!" + helperhandler.cause().getMessage()));
            }
        });
    }


    //TODO use list all datasets instead of pulling every dataset
    private void listAllDatasetsAndIndex(AtomicBoolean verbose, CommandProcess process, String
            catalogId, Handler<Future<Void>> listHandler) {
        DatasetsService datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
        CataloguesService cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);

        cataloguesService.getCatalogue(catalogId, "application/json", catalogueHandler -> {
            if (catalogueHandler.succeeded()) {

                CatalogueHelper catalogueHelper = new CatalogueHelper(catalogId, "application/json", catalogueHandler.result().getString("content"));
                catalogueHelper.model(helperhandler -> {
                    if (helperhandler.succeeded()) {
                        List<JsonObject> datasetsWithEverything = new ArrayList<>();
                        List<Future> getDataFutures = new ArrayList<>();

                        catalogueHelper.getModel().listSubjects().forEachRemaining(iterator -> iterator.listProperties(DCAT.dataset).forEachRemaining(laufer -> {
                            String datasetId = laufer.getResource().getURI().substring(laufer.getResource().getURI().indexOf("data/") + 5);
                            PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(datasetId,catalogId,getClass());
                            Future getDatasetFuture = Future.future();
                            getDataFutures.add(getDatasetFuture);

                            datasetsService.getDataset(datasetId, catalogId, "application/n-triples", getHandler -> {
                                if (getHandler.succeeded()) {
                                    JsonObject datasetJson = new JsonObject().put("datasetId", datasetId)
                                            .put("catalogId", catalogId)
                                            .put("dataset", getHandler.result().getString("content"));
                                    datasetsWithEverything.add(datasetJson);
                                    getDatasetFuture.complete();
                                } else {
                                    LOGGER.error("Problem getting the dataset : " + getHandler.cause().getMessage());
                                    getDatasetFuture.fail("Problem getting the dataset : " + getHandler.cause().getMessage());
                                }
                            });

                        }));
                        CompositeFuture.join(getDataFutures).setHandler(futureHandler -> {
                            indexDatasets(datasetsWithEverything, verbose.get(), process, indexHandler -> {
                                if (indexHandler.failed()) {
                                    listHandler.handle(Future.failedFuture("There was a problem reindexing the datasets " + indexHandler.cause().getMessage()));
                                } else {
                                    listHandler.handle(Future.succeededFuture());
                                }
                            });
                        });
                    } else {
                        listHandler.handle(Future.failedFuture("Problem with the cataloghelper." + helperhandler.cause().getMessage()));
                    }
                });
            } else {
                listHandler.handle(Future.failedFuture("Problem getting the list of datasets." + catalogueHandler.cause().getMessage()));
            }
        });
    }

    private void clearIndex(IndexService indexService, List<String> allDatasets, CommandProcess process,
                            int currentpage, Handler<Future<Void>> clearhandler) {

        int indexLimit = 1000;

        indexService.listAllDatasets(indexLimit, currentpage, listHandler -> {
            if (listHandler.succeeded()) {
                String[] listOfIds = Json.decodeValue(listHandler.result().getJsonArray("listDatasets").toString(), String[].class);

                for (String id : listOfIds) {
                    if (!allDatasets.contains(id)) {
                        indexService.deleteDataset(id, deletehandler -> {
                            PiveauLogger LOGGER = PiveauLoggerFactory.getDatasetLogger(id,getClass());
                            if (deletehandler.failed()) {
                                LOGGER.error("Problem cleaning the index for dataset with id : " + id + " because of " + deletehandler.cause().getMessage());
                            }
                        });
                    }
                }
                if (listOfIds.length == indexLimit) {
                    clearIndex(indexService, allDatasets, process, currentpage + 1, clearhandler);
                } else {
                    clearhandler.handle(Future.succeededFuture());
                }
            } else {
                process.write("Problem getting all datasets from the searchIndex. \n" + listHandler.cause().getMessage());
            }
        });
    }


    /**
     * @return Future
     * <p>
     * Main helper function to index a dataset provided by a datasetID and catalogID
     */

    //TODO bei performance problemen die futures nicht sammeln und direkt succeeden!
    private void indexDatasets(List<JsonObject> datasetsWithCatalogId, boolean verbose, CommandProcess
            process, Handler<Future<Void>> listHandler) {

        IndexService indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS);
        JsonObject config = config();
        int packageSize = 100;

        int start = 0;
        int end = packageSize;

        List<Future> allDataFuturesList = new ArrayList<>();

        for (int i = 0; i <= (datasetsWithCatalogId.size() / packageSize); i++) {
            if (end > datasetsWithCatalogId.size()) {
                end = datasetsWithCatalogId.size();
            }

            Future<Void> allDataFuture = Future.future();
            allDataFuturesList.add(allDataFuture);

            JsonObject[] datasetStrings = datasetsWithCatalogId.subList(start, end).toArray(new JsonObject[0]);
            JsonArray datasetsForIndex = new JsonArray();

            List<Future> packageFuturesList = new ArrayList<>();

            for (int k = 0; k < datasetStrings.length; k++) {

                Future<Void> packageFuture = Future.future();
                packageFuturesList.add(packageFuture);
                if (verbose) {
                    process.write("Currently indexing dataset with id" + datasetStrings[k].getString("datasetId") + "\n");
                }
                PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(datasetStrings[k].getString("datasetId"),datasetStrings[k].getString("catalogId"),getClass());
                DatasetHelper.create(datasetStrings[k].getString("datasetId"), datasetStrings[k].getString("dataset"), Lang.NTRIPLES.getHeaderString(), null, datasetStrings[k].getString("catalogId"), ar -> {
                    if (ar.succeeded()) {

                        DatasetToIndexConverter datasetToIndexConverter = new DatasetToIndexConverter(config);
                        JsonObject indexMessage = datasetToIndexConverter.convert2(ar.result());
                        datasetsForIndex.add(indexMessage);
                        packageFuture.complete();
                    } else {

                        LOGGER.error("Problem creating the datasetToIndex with datasetHelper : " + ar.cause());
                        packageFuture.fail("Problem creating the datasetToIndex with datasetHelper : " + ar.cause());
                    }
                });
            }
            PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());
            CompositeFuture.join(packageFuturesList).setHandler(packageHandler -> {
                if (packageHandler.succeeded()) {
                    indexService.addDatasetWithoutCB(new JsonObject().put("datasets", datasetsForIndex), indexHandler -> {
                        if (indexHandler.failed()) {
                            LOGGER.error("Problem indexing datasets " + indexHandler.cause().getMessage() + "\n");
                            allDataFuture.complete();
                        } else {
                            allDataFuture.complete();
                        }
                    });
                } else {
                    indexService.addDatasetWithoutCB(new JsonObject().put("datasets", datasetsForIndex), indexHandler -> {
                        if (indexHandler.failed()) {
                            LOGGER.error("Problem indexing datasets " + indexHandler.cause().getMessage() + "\n");
                            allDataFuture.complete();
                        } else {
                            allDataFuture.complete();
                        }
                    });
                }
            });
            start = end;
            end = end + packageSize;
        }
        CompositeFuture.join(allDataFuturesList).setHandler(allDataHandler -> {
            if (allDataHandler.succeeded()) {
                listHandler.handle(Future.succeededFuture());
            } else {
                PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());
                LOGGER.error("Problem indexing all datasets " + allDataHandler.cause().getMessage() + "\n");
                listHandler.handle(Future.succeededFuture());
            }
        });
    }

    private void getAllDatasetIds(Handler<AsyncResult<ArrayList<String>>> handler) {

        ArrayList<String> listOfAllIds = new ArrayList<>();

        CataloguesService cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);
        IndexService indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS);


        cataloguesService.listCatalogues("application/json", null, 0, catalogueHandler -> {
            if (catalogueHandler.succeeded()) {

                //TODO Getting the catalog id is not supported yet so this hack takes the id out of the URI which is bad!
                JsonObject listOfCatalogs = new JsonObject(catalogueHandler.result().getString("content"));
                JsonArray catalogueList = listOfCatalogs.getJsonObject("results").getJsonArray("bindings");

                ArrayList<String> listOfCatalogIds = new ArrayList<>();

                indexService.listAllCIds(1000, 0, CIDhandler -> {
                    if (CIDhandler.succeeded()) {
                        String[] listOfIds = Json.decodeValue(CIDhandler.result().getJsonArray("listCatalogues").toString(), String[].class);

                        for (Object catalog : catalogueList) {

                            //TODO Getting the catalog id is not supported yet so this hack takes the id out of the URI which is bad!
                            JsonObject JsonCatalog = (JsonObject) catalog;
                            String catalogURI = JsonCatalog.getJsonObject("g").getString("value");
                            String catalogId = catalogURI.substring(catalogURI.indexOf("catalogue/") + 10);
                            listOfCatalogIds.add(catalogId);

                            cataloguesService.getCatalogue(catalogId, "application/json", cHandler -> {
                                if (catalogueHandler.succeeded()) {

                                    CatalogueHelper catalogueHelper = new CatalogueHelper(catalogId, "application/json", cHandler.result().getString("content"));
                                    catalogueHelper.model(helperhandler -> {
                                        if (helperhandler.succeeded()) {
                                            catalogueHelper.getModel().listSubjects().forEachRemaining(iterator -> iterator.listProperties(DCAT.dataset).forEachRemaining(laufer -> {
                                                String datasetId = laufer.getResource().getURI().substring(laufer.getResource().getURI().indexOf("data/") + 5);
                                                listOfAllIds.add(datasetId);
                                            }));
                                        } else {
                                            handler.handle(Future.failedFuture("Problem with the cataloghelper." + helperhandler.cause().getMessage()));
                                        }
                                    });
                                } else {
                                    handler.handle(Future.failedFuture("Problem getting the list of datasets." + catalogueHandler.cause().getMessage()));
                                }
                            });
                        }


                        //Delete all catalogues which are not needed!
                        for (int inter = 0; inter < listOfIds.length; inter++) {
                            PiveauLogger LOGGER = PiveauLoggerFactory.getCatalogueLogger(listOfIds[inter],getClass());
                            if (!(listOfCatalogIds.contains(listOfIds[inter]))) {
                                indexService.deleteCatalog(listOfIds[inter], deleteHandler -> {

                                    if (deleteHandler.failed()) {
                                        LOGGER.error("Problem deleting an non existend Catalog!");
                                    } else {
                                        LOGGER.info("Successfully deleted catalogue");
                                    }
                                });
                            }
                        }
                    } else {
                        PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());
                        LOGGER.error("Problem getting all catalogues");
                    }
                });
                handler.handle(Future.succeededFuture(listOfAllIds));
            }
        });

    }

    /**
     * @param cliName
     * @param arrayOfArguments
     * @return CLI
     * <p>
     * Creates a new Cli with a given list of arguments in order to use in the shell.
     */
    private CLI createCLI(String cliName, ArrayList<Argument> arrayOfArguments) {
        CLI cli = CLI.create(cliName);
        for (Argument argument : arrayOfArguments) {
            cli.addArgument(argument);
        }
        return cli;
    }

    /**
     * @param name
     * @param description
     * @param defaultValue
     * @param required
     * @return Argument
     * <p>
     * Creates an Argument in order to use in an CLI for the shell.
     */
    private Argument createArgument(String name, String description, String defaultValue, boolean required) {
        Argument argument = new Argument();
        argument.setDescription(description);
        argument.setDefaultValue(defaultValue);
        argument.setArgName(name);
        argument.setRequired(required);
        return argument;
    }
}
