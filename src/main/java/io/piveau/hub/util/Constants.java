package io.piveau.hub.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final public class Constants {


    // IndexVerticle Event Bus Constants
    static public final String INDEX_ADD_DATASET = "index-add-dataset";
    static public final String INDEX_ADD_CATALOG = "index-add-catalog";

    static public final String ENV_HUB_TRIPLESTORE_CONFIG ="HUB_TRIPLESTORE_CONFIG";
    static public final String ENV_HUB_SERVICE_PORT = "HUB_SERVICE_PORT";
    static public final String ENV_HUB_API_KEY = "HUB_API_KEY";
    static public final String ENV_HUB_BASE_URI = "HUB_BASE_URI";
    static public final String ENV_HUB_VALIDATOR = "HUB_VALIDATOR";
    static public final String ENV_HUB_SEARCH_SERVICE = "HUB_SEARCH_SERVICE";
    static public final String ENV_HUB_SEARCH_CLI_CONFIG = "HUB_SEARCH_CLI_CONFIG";
    static public final String ENV_TRANSLATION_SERVICE = "HUB_TRANSLATION_SERVICE";
    static public final String ENV_DATA_UPLOAD = "HUB_DATA_UPLOAD";
    static public final String ENV_HUB_LOAD_VOCABULARY = "HUB_LOAD_VOCABULARY";

    public static final List<String> ALLOWED_CONTENT_TYPES = Collections.unmodifiableList(Arrays.asList(
        "application/rdf+xml",
        "application/ld+json",
        "text/turtle",
        "text/n3",
        "application/trig",
        "application/n-triples"
    ));

}
