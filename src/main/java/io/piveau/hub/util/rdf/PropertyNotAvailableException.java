package io.piveau.hub.util.rdf;

public class PropertyNotAvailableException extends Exception {

    public PropertyNotAvailableException() {
        super();
    }

    public PropertyNotAvailableException(String message) {
        super(message);
    }

}
