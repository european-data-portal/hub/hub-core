package io.piveau.hub;

import io.piveau.hub.converters.CatalogToIndexConverter;
import io.piveau.hub.converters.DatasetToIndexConverter;
import io.piveau.hub.services.datasets.DatasetsServiceVerticle;
import io.piveau.hub.util.DCATAPUriSchema;
import io.piveau.hub.util.DatasetHelper;
import io.piveau.hub.util.rdf.DCATAP;
import io.piveau.hub.util.rdf.VocabularyManager;
import io.piveau.utils.JenaUtils;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.DCTerms;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;


@RunWith(VertxUnitRunner.class)
public class DatasetToIndexTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatasetToIndexTest.class);
    private Vertx vertx;

//    @BeforeClass
//    public static void globalSetup(){
//        rdfUtil = new RDFUtil("http://test.de");
//        vertx = Vertx.vertx();
//
//
//        VocabularyManager.init(new JsonObject().put("PIVEAU_HUB_LOAD_VOCABULARY", true), vertx);
//        vocabularyManager = VocabularyManager.getInstance();
//        Future<Void> f2 = vocabularyManager.loadVocabularies();
//        // Super hacky, but working
//        while(!f2.isComplete()){}
//
//
//        DatasetToIndexConverter.init(rdfUtil, new JsonObject().put("PIVEAU_HUB_LOAD_VOCABULARY", false));
//        datasetToIndexConverter = DatasetToIndexConverter.getInstance();
//        Future<Void> f = datasetToIndexConverter.loadVocabulary(vertx);
//
//        LOGGER.info("Global setup finished");
//    }

    @Before
    public void setUp(TestContext tc) {
        this.vertx = Vertx.vertx();
        VocabularyManager.init(new JsonObject().put("PIVEAU_HUB_LOAD_VOCABULARY", true), vertx, tc.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext tc) {
        vertx.close(tc.asyncAssertSuccess());
    }

    @Test
    public void testExampleDataset(TestContext tc) {
        DCATAPUriSchema.config(new JsonObject().put("baseUri", "https://europeandataportal.eu/"));
        FileSystem fileSystem = vertx.fileSystem();
        Buffer buffer = fileSystem.readFileBlocking("doc/example_dataset.ttl");
        DatasetHelper.create("test-dataset", buffer.toString(), "text/turtle", null, "cat1", ar -> {
            if (ar.succeeded()) {
                JsonObject result = new DatasetToIndexConverter(new JsonObject().put("PIVEAU_HUB_LOAD_VOCABULARY", false)).convert2(ar.result());
                tc.assertFalse(result.isEmpty());
                LOGGER.info(result.encodePrettily());
                tc.assertTrue(true);
            } else {
                tc.fail(ar.cause());
            }
        });
    }

    @Test
    public void testExampleCatalog(TestContext tc) {
        DCATAPUriSchema.config(new JsonObject().put("baseUri", "https://europeandataportal.eu/"));
        FileSystem fileSystem = vertx.fileSystem();
        Buffer buffer = fileSystem.readFileBlocking("doc/example_catalog.ttl");
        Model model = JenaUtils.read(buffer.getBytes(), "text/turtle");
        JsonObject result = new CatalogToIndexConverter().convert(model, "test-catalog");
        tc.assertFalse(result.isEmpty());
        LOGGER.info(result.encodePrettily());
        tc.assertTrue(true);
    }

    //@Test
    public void testTestDataset(TestContext tc) {
        DCATAPUriSchema.config(new JsonObject().put("baseUri", "https://europeandataportal.eu/"));
        FileSystem fileSystem = vertx.fileSystem();
        Buffer buffer = fileSystem.readFileBlocking("doc/test.ttl");
        DatasetHelper.create("a-trust-registrierungsstellen", buffer.toString(), "text/turtle", null, "cat1", ar -> {
            if (ar.succeeded()) {
                JsonObject result = new DatasetToIndexConverter(new JsonObject().put("PIVEAU_HUB_LOAD_VOCABULARY", false)).convert2(ar.result());
                tc.assertFalse(result.isEmpty());
                LOGGER.info(result.encodePrettily());
                tc.assertTrue(true);
            } else {
                tc.fail(ar.cause());
            }
        });
    }



}
