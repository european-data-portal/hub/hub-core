package io.piveau.hub.util.rdf;

import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateTimeUtil {


    private String[] knownDateTimePatterns() {
       String[] patterns = {
           "yyyy-MM-dd'T'HH:mm:ss",
           "yyyy-MM-dd'T'HH:mm:ss'Z'",
           "yyyy-MM-dd'T'HH:mm:ss.S",
           "yyyy-MM-dd'T'HH:mm:ss.SS",
           "yyyy-MM-dd'T'HH:mm:ss.SSS",
           "yyyy-MM-dd'T'HH:mm:ss.SSSS",
           "yyyy-MM-dd'T'HH:mm:ss.SSSSS",
           "yyyy-MM-dd'T'HH:mm:ss.SSSSSS",
       };
       return patterns;
    }

    private String[] knownDatePatterns() {
        String[] patterns = {
            "yyyy-MM-dd",
            "yyyy/MM/dd",
        };
        return patterns;
    }

    /**
     *
     * Target RexEx: ^\\d{4}-(?:0[0-9]{1}|1[0-2]{1})-(0?[1-9]|[12][0-9]|3[01])[tT ]\\d{2}:\\d{2}:\\d{2}(\\.\\d+)?([zZ]|[+-]\\d{2}:\\d{2})$
     *
     */
    public String parse(String inputDate) {
        PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());
        DateTimeFormatter targetFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateTimeFormatter formatter;

        for(String p : knownDateTimePatterns()) {
            formatter = DateTimeFormatter.ofPattern(p);
            try {
                String text = LocalDateTime.parse(inputDate, formatter).format(targetFormatter);
                return text;
            } catch (DateTimeParseException ex) {
                //LOGGER.info(ex.getMessage());
            } catch (RuntimeException ex) {
                //LOGGER.info(ex.getMessage());
            }
        }

        for(String p : knownDatePatterns()) {
            formatter = DateTimeFormatter.ofPattern(p);
            try {
                LocalDateTime date = LocalDate.parse(inputDate, formatter).atStartOfDay();
                String text = date.format(targetFormatter);
                System.out.println(text);
                return text;
            } catch (DateTimeParseException ex) {
                //LOGGER.info(ex.getMessage());
            } catch (RuntimeException ex) {
                //LOGGER.info(ex.getMessage());
            }
        }
        LOGGER.error("Could not parse date " + inputDate);
        return null;
    }

    public String now() {
        DateTimeFormatter targetFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime date = LocalDateTime.now();
        return date.format(targetFormatter);
    }
}
