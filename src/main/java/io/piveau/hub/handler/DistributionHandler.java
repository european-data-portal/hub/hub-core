package io.piveau.hub.handler;

import io.piveau.hub.services.distributions.DistributionsService;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class DistributionHandler {


    private DistributionsService distributionsService;


    public DistributionHandler(Vertx vertx, String address){
        distributionsService= DistributionsService.createProxy(vertx, address);
    }

    public void handleGetDistribution(RoutingContext context){
        String id = context.pathParam("id");
        String acceptType = context.getAcceptableContentType();

        distributionsService.getDistribution(id, acceptType, ar->{
            if (ar.succeeded()) {
                JsonObject result = ar.result();
                switch (result.getString("status")) {
                    case "success":
                        context.response().putHeader("Content-Type", result.getString("contentType")).end(result.getString("content"));
                        break;
                    case "not found":
                        context.response().setStatusCode(404).end();
                        break;
                    default:
                        context.response().setStatusCode(400).end();
                }
            } else {
                context.response().setStatusCode(500).end(ar.cause().getMessage());
            }
        });

    }

    public void handlePostDistribution(RoutingContext context){

    }

    public void handleDeleteDistribution(RoutingContext context){

    }


}
