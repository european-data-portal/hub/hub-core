package io.piveau.hub.util;

import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.piveau.hub.util.rdf.DCATAP;
import io.piveau.hub.util.rdf.SPDX;
import io.piveau.utils.Hash;
import io.piveau.utils.JenaUtils;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.util.ResourceUtils;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class DatasetHelper {
    private String id;
    private String hash;
    private String catalogueId;

    private String sourceType;

    private DCATAPUriSchema uriSchema;

    private Model model;

    public static void create(String content, String contentType, Handler<AsyncResult<DatasetHelper>> handler) {
        new DatasetHelper(content, contentType, handler);
    }

    public static void create(String id, String content, String contentType, String hash, String catalogueId, Handler<AsyncResult<DatasetHelper>> handler) {
        new DatasetHelper(id, content, contentType, hash, catalogueId, handler);
    }

    private DatasetHelper(String content, String contentType, Handler<AsyncResult<DatasetHelper>> handler) {
        hash = Hash.asHexString(content);
        try {
            model = JenaUtils.read(content.getBytes(), contentType);
            extractId();
            handler.handle(Future.succeededFuture(this));
        } catch (Exception e) {
            handler.handle(Future.failedFuture(e));
        }
    }

    private DatasetHelper(String id, String content, String contentType, String hash, String catalogueId, Handler<AsyncResult<DatasetHelper>> handler) {
        this(content, contentType, ar -> {
            if (ar.succeeded()) {
                DatasetHelper helper = ar.result();
                helper.id = id;

                if (helper.uriSchema == null) {
                    helper.uriSchema = DCATAPUriSchema.applyFor(id);
                }

                helper.catalogueId = catalogueId;
                if (hash != null && !hash.isEmpty()) {
                    helper.hash = hash;
                }
                handler.handle(Future.succeededFuture(helper));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });

    }

    public String id() {
        return id;
    }

    public String catalogueId() {
        return catalogueId;
    }

    public String hash() {
        return hash;
    }

    public String graphName() {
        return uriSchema.datasetGraphName();
    }

    public String uriRef() {
        return uriSchema.datasetUriRef();
    }

    public String recordUriRef() {
        return uriSchema.recordUriRef();
    }

    public String catalogueUriRef() {
        return DCATAPUriSchema.applyFor(catalogueId).catalogueUriRef();
    }

    public String catalogueGraphName() {
        return DCATAPUriSchema.applyFor(catalogueId).catalogueGraphName();
    }

    public String validationUriRef() {
        return uriSchema.validationUriRef();
    }

    public String validationGraphName() {
        return uriSchema.validationGraphName();
    }

    public Resource resource() {
        return model.getResource(uriRef());
    }

    public Resource recordResource() {
        return model.getResource(recordUriRef());
    }

    public void sourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String sourceType() {
        return sourceType;
    }

    public Model model() {
        return model;
    }

    public String stringify(Lang lang) {
        return JenaUtils.write(model, lang);
    }

    public void update(Model oldModel, String recordUriRef) {
        uriSchema = DCATAPUriSchema.applyFor(DCATAPUriSchema.parseUriRef(recordUriRef).id());

        Model recordModel = JenaUtils.extractResource(oldModel.getResource(recordUriRef));
        Resource record = recordModel.getResource(recordUriRef);
        updateRecord(record, hash);

        Map<String, Map.Entry<String, Statement>> dist = saveDistributions(oldModel);
        renameReferences(dist);


        Model translationModel = JenaUtils.extractResource(oldModel.getResource(uriRef()));
        this.extractTranslations(translationModel, oldModel);
        model.add(translationModel);


        model.add(recordModel);
    }

    public void init(String normalized) {
        uriSchema = DCATAPUriSchema.applyFor(normalized);
        renameReferences(Collections.emptyMap());
        initRecord(model.createResource(recordUriRef(), DCAT.CatalogRecord));
    }

    public void setAccessURLs(DataUploadConnector dataUploadConnector) {
        model.listResourcesWithProperty(RDF.type, DCAT.Distribution).forEachRemaining(resource -> {
            String distId = DCATAPUriSchema.parseUriRef(resource.getURI()).id();
            if(resource.hasProperty(DCAT.accessURL)) {
                resource.removeAll(DCAT.accessURL);
                resource.addProperty(DCAT.accessURL, model.createResource(dataUploadConnector.getDataURL(distId)));
            } else {
                resource.addProperty(DCAT.accessURL, model.createResource(dataUploadConnector.getDataURL(distId)));
            }
        });
    }

    private void initRecord(Resource record) {
        record.addProperty(FOAF.primaryTopic, record.getModel().createResource(uriRef()));
        record.addProperty(DCTerms.created, ZonedDateTime
                .now(ZoneOffset.UTC)
                .truncatedTo(ChronoUnit.SECONDS)
                .format(DateTimeFormatter.ISO_DATE_TIME), XSDDatatype.XSDdateTime);

        record.addProperty(DCTerms.modified, ZonedDateTime
                .now(ZoneOffset.UTC)
                .truncatedTo(ChronoUnit.SECONDS)
                .format(DateTimeFormatter.ISO_DATE_TIME), XSDDatatype.XSDdateTime);

        record.addProperty(DCTerms.identifier, id);

        Resource checksum = record.getModel().createResource(SPDX.Checksum);
        checksum.addProperty(SPDX.algorithm, SPDX.checksumAlgorithm_md5);
        checksum.addProperty(SPDX.checksumValue, hash);
        record.addProperty(SPDX.checksum, checksum);
    }

    private void updateRecord(Resource record, String hash) {
        Resource checksum = record.getPropertyResourceValue(SPDX.checksum);
        checksum.removeAll(SPDX.checksumValue);
        checksum.addProperty(SPDX.checksumValue, hash);

        record.removeAll(DCTerms.modified);
        record.addProperty(DCTerms.modified, ZonedDateTime
                .now(ZoneOffset.UTC)
                .truncatedTo(ChronoUnit.SECONDS)
                .format(DateTimeFormatter.ISO_DATE_TIME), XSDDatatype.XSDdateTime);
    }

    private void renameReferences(Map<String, Map.Entry<String, Statement>> identDist) {
        //rename datasets
        model.listSubjectsWithProperty(RDF.type, DCAT.Dataset).forEachRemaining(ds -> ResourceUtils.renameResource(ds, uriRef()));

        //rename distributions
        model.listResourcesWithProperty(RDF.type, DCAT.Distribution).forEachRemaining(resource -> {

            //set the id to lookup in the identDist map
            String id = "";
            if (resource.hasProperty(DCTerms.identifier)) {
                id = resource.getProperty(DCTerms.identifier).getObject().toString();
            }else{
                id=resource.getURI();
            }

            //if the Distribution in the new model has an {@link DCTerms#identifier identifier} that was already present in the old model,
            //      give the new Distribution the same id/uriRef as the old distribution
            //else give it a new uriRef
            //
            if (identDist.containsKey(id)) {
                Resource renamed = ResourceUtils.renameResource(resource, identDist.get(id).getKey());
                renamed.addProperty(DCTerms.identifier, identDist.get(id).getValue().getObject());
            } else {
                //if the new has an {@link DCTerms#identifier identifier}, which is not one found in the old model, use this
                // else create a new one
                String newID = "";
                if (resource.hasProperty(DCTerms.identifier)) {
                    newID = JenaUtils.normalize(resource.getProperty(DCTerms.identifier).getObject().toString());
                } else {
                    newID = UUID.randomUUID().toString();
                }

                Resource renamed = ResourceUtils.renameResource(resource, DCATAPUriSchema.applyFor(newID).distributionUriRef());
                // if there is no {@link DCTerms#identifier identifier}, add a the new one we can use on the next update
                if (!renamed.hasProperty(DCTerms.identifier)) {
                    renamed.addProperty(DCTerms.identifier, resource);
                }
            }
        });
    }

    /**
     This method collects all
     @param oldModel the old Model, from which the
     @return A mapping from the distribution identifier to the distribution uri which looks like:
          Map< Distribution {@link DCTerms#identifier identifier} as string, Map < Distribution Uri, Distribution {@link DCTerms#identifier identifier} as Statement>>
     */
    private Map<String, Map.Entry<String, Statement>> saveDistributions(Model oldModel) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(this, getClass());
        Map<String, Map.Entry<String, Statement>> identDist = new HashMap<>();

        oldModel.listSubjects().forEachRemaining(s -> {
            s.listProperties(DCAT.distribution).forEachRemaining(dist -> {
                dist.getObject().asResource().listProperties(DCTerms.identifier).forEachRemaining(i -> {
                    identDist.put(i.getObject().toString(), new AbstractMap.SimpleEntry<>(dist.getObject().toString(), i));
                    log.info("Id: {} in Dist: {}",i.getObject().toString(), dist.getObject().toString());
                });
            });
        });
        return identDist;
    }

    private void extractId() {
        ResIterator it = model.listSubjectsWithProperty(RDF.type, DCAT.CatalogRecord);
        if (it.hasNext()) {
            Resource record = it.next();
            id = record.getProperty(DCTerms.identifier).getObject().asLiteral().getString();
        }
        it = model.listSubjectsWithProperty(RDF.type, DCAT.Dataset);
        if (it.hasNext()) {
            Resource dataset = it.next();
            if (dataset.getURI() != null) {
                DCATAPUriSchema tmp = DCATAPUriSchema.parseUriRef(dataset.getURI());
                if (tmp != null) {
                    uriSchema = DCATAPUriSchema.applyFor(tmp.id());
                }
            }
        }
    }


    private void extractTranslations(Model translationModel, Model oldModel) {
        Resource oldResource = oldModel.getResource(uriRef());
        Resource newResource = translationModel.getResource(uriRef());
        translationModel.removeAll();
        this.extractTranslationFromResource(oldResource, newResource, DCTerms.title);
        this.extractTranslationFromResource(oldResource, newResource, DCTerms.description);
//        this.extractDistributions(oldResource, newResource, translationModel);
//        log.debug(RDFUtil.serializeModel(translationModel));
    }


    private void extractDistributions(Resource oldResource, Resource newResource, Model translationModel) {
        StmtIterator iterator = oldResource.listProperties(DCATAP.dcatDistribution);
        while (iterator.hasNext()) {
            Statement statement = iterator.nextStatement();
            if (statement.getSubject().isResource()) {
                Resource distribution = statement.getResource();
                String distributionUri = distribution.getURI();
                Resource newDistribution = translationModel.createResource(distributionUri);
                newResource.addProperty(DCATAP.dcatDistribution, newDistribution);
                this.extractTranslationFromResource(distribution, newDistribution, DCTerms.title);
                this.extractTranslationFromResource(distribution, newDistribution, DCTerms.description);
            }
        }
    }

    private void extractTranslationFromResource(Resource oldResource, Resource newResource, Property property) {
        StmtIterator iterator = oldResource.listProperties(property);
        while (iterator.hasNext()) {
            Statement statement = iterator.nextStatement();
            Literal literal = statement.getLiteral();
            String lang = literal.getLanguage();
            if(lang.contains("mtec")) {
                newResource.addLiteral(property, literal);
            }
        }
    }
}
