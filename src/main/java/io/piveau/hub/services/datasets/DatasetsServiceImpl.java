package io.piveau.hub.services.datasets;

import io.piveau.hub.converters.DatasetToIndexConverter;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.services.translation.TranslationService;
import io.piveau.hub.services.validation.ValidationServiceVerticle;
import io.piveau.hub.util.*;
import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.piveau.hub.util.rdf.SPDX;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import org.apache.http.HttpHeaders;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;

import java.io.ByteArrayInputStream;
import java.util.concurrent.atomic.AtomicInteger;

public class DatasetsServiceImpl implements DatasetsService {

    private TSConnector connector;
    private DataUploadConnector dataUploadConnector;
    private JsonObject config;
    private Vertx vertx;
    private IndexService indexService;
    private TranslationService translationService;

    DatasetsServiceImpl(TSConnector connector, DataUploadConnector dataUploadConnector, JsonObject config, Vertx vertx, Handler<AsyncResult<DatasetsService>> readyHandler) {
        this.vertx = vertx;
        this.connector = connector;
        this.dataUploadConnector = dataUploadConnector;
        this.config = config;
        this.indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS);
        this.translationService = TranslationService.createProxy(vertx, TranslationService.SERVICE_ADDRESS);
        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public DatasetsService listDatasets(String consumes, String catalogueId, Integer limit, Integer offset, Boolean sourceIds, Handler<AsyncResult<JsonObject>> handler) {
        String catalogueUriRef = catalogueId != null && !catalogueId.isEmpty() ? DCATAPUriSchema.applyFor(catalogueId).catalogueUriRef() : null;

        if (sourceIds) {
            connector.listDatasetSources(catalogueUriRef, ar -> {
                if (ar.succeeded()) {
                    handler.handle(Future.succeededFuture(new JsonObject()
                            .put("contentType", "application/json")
                            .put("status", "success")
                            .put("content", ar.result())
                    ));
                } else {
                    handler.handle(Future.failedFuture(ar.cause()));
                }
            });
        } else {
            connector.listDatasets(consumes, catalogueUriRef, limit, offset, ar -> {
                if (ar.succeeded()) {
                    handler.handle(Future.succeededFuture(new JsonObject()
                            .put("contentType", consumes != null ? consumes : "application/json")
                            .put("status", "success")
                            .put("content", ar.result())
                    ));
                } else {
                    handler.handle(Future.failedFuture(ar.cause()));
                }
            });
        }

        return this;
    }

    @Override
    public DatasetsService getDataset(String datasetId, String catalogueId, String consumes, Handler<AsyncResult<JsonObject>> handler) {
        Future<JsonObject> uriFuture = Future.future();
        connector.getDatasetUriRefs(datasetId, catalogueId, uriFuture);
        uriFuture.compose(uriRefs -> {
            Future<String> graphFuture = Future.future();
            String datasetUriRef = uriRefs.getString("datasetUriRef");
            connector.getGraph(DCATAPUriSchema.parseUriRef(datasetUriRef).datasetGraphName(), consumes, graphFuture);
            return graphFuture;
        }).setHandler(ar -> {
            if (ar.succeeded()) {

                // we should remove record here

                handler.handle(Future.succeededFuture(new JsonObject()
                        .put("status", "success")
                        .put("content", ar.result())
                ));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService getRecord(String datasetId, String catalogueId, String consumes, Handler<AsyncResult<JsonObject>> handler) {
        connector.getDatasetUriRefs(datasetId, catalogueId, ar -> {
            if (ar.succeeded()) {
                JsonObject result = ar.result();
                String datasetUriRef = result.getString("datasetUriRef");
                String recordUriRef = result.getString("recordUriRef");
                DCATAPUriSchema uriSchema = DCATAPUriSchema.parseUriRef(datasetUriRef);
                connector.getRecord(uriSchema.datasetGraphName(), recordUriRef, consumes, rr -> {
                    if (rr.succeeded()) {
                        handler.handle(Future.succeededFuture(new JsonObject()
                                .put("status", "success")
                                .put("content", rr.result())
                        ));
                    } else {
                        handler.handle(Future.failedFuture(rr.cause()));
                    }
                });
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService putDataset(String datasetId, String dataset, String contentType, String catalogueId, String hash, Boolean createAccessURLs, Handler<AsyncResult<JsonObject>> handler) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(datasetId,catalogueId,getClass());
        DatasetHelper.create(datasetId, dataset, contentType, hash, catalogueId, dr -> {
            if (dr.succeeded()) {
                DatasetHelper datasetHelper = dr.result();
                Future<String> existsFuture = Future.future();
                connector.catalogueExists(datasetHelper.catalogueUriRef(), existsFuture);
                existsFuture.compose(type -> {
                    Future<JsonObject> hashFuture = Future.future();
                    datasetHelper.sourceType(type);
                    getHash(datasetHelper, hashFuture);
                    return hashFuture;
                }).compose(hr -> {
                    Future<Void> createOrUpdateFuture = Future.future();
                    if (hr.getBoolean("success") && hr.getString("hash").equals(datasetHelper.hash())) {
                        log.debug("hash equal, skipping");
                        // self repair...
                        catalogue(datasetHelper);
                        createOrUpdateFuture.fail("skipped");
                    } else {
                        if (hr.getBoolean("success")) {
                            log.debug("update");
                            String recordUriRef = hr.getString("recordUriRef");
                            connector.getGraph(DCATAPUriSchema.parseUriRef(recordUriRef).datasetGraphName(), ar -> {
                                if (ar.succeeded()) {
                                    datasetHelper.update(ar.result(), recordUriRef);
                                    createOrUpdateFuture.complete();
                                } else {
                                    createOrUpdateFuture.fail(ar.cause());
                                }
                            });
                        } else {
                            log.debug("create");
                            connector.findFreeNormalized(datasetHelper, new AtomicInteger(0), ar -> {
                                if (ar.succeeded()) {
                                    datasetHelper.init(ar.result());
                                    if(createAccessURLs) {
                                        datasetHelper.setAccessURLs(dataUploadConnector);
                                    }
                                    createOrUpdateFuture.complete();
                                } else {
                                    createOrUpdateFuture.fail(ar.cause());
                                }
                            });
                        }
                    }
                    return createOrUpdateFuture;
                }).compose(v -> {
                    Future<JsonObject> storeFuture = Future.future();
                    store(datasetHelper, storeFuture);
                    return storeFuture;
                }).setHandler(ar -> {
                    if (ar.succeeded()) {

                        catalogue(datasetHelper);
                        index(datasetHelper);
                        translate(datasetHelper);

                        if (datasetHelper.sourceType().equals("dcat-ap")) {
                            validate(datasetHelper);
                        }

                        handler.handle(Future.succeededFuture(ar.result()));
                    } else {
                        log.warn("Dataset preparation exited because ", ar.cause());
                        handler.handle(Future.failedFuture(ar.cause()));
                    }
                });
            } else {
                handler.handle(Future.failedFuture(dr.cause()));
            }
        });
        return this;
    }

    @Override
    @Deprecated
    public DatasetsService postDataset(String dataset, String contentType, Handler<AsyncResult<JsonObject>> handler) {
        handler.handle(Future.failedFuture("Not yet implemented"));
        return this;
    }

    @Override
    public DatasetsService deleteDataset(String datasetId, String catalogueId, Handler<AsyncResult<JsonObject>> handler) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(datasetId,catalogueId,getClass());
        connector.getDatasetUriRefs(datasetId, catalogueId, ar -> {
            if (ar.succeeded()) {
                JsonObject result = ar.result();
                String datasetUriRef = result.getString("datasetUriRef");
                connector.deleteGraph(datasetUriRef, dr -> {
                    if (dr.succeeded()) {
                        indexService.deleteDataset(datasetId, ir -> {
                            if (ir.failed()) {
                                log.error("Remove index", ir.cause());
                            }
                        });
                        handler.handle(Future.succeededFuture(new JsonObject().put("status", "deleted")));
                    } else {
                        handler.handle(Future.failedFuture(ar.cause()));
                    }
                });
                if (result.containsKey("validationUriRef")) {
                    String validationUriRef = result.getString("validationUriRef");
                    connector.deleteGraph(validationUriRef, vr -> {
                        if (vr.failed()) {
                            log.warn("Delete validation graph", vr.cause());
                        }
                    });
                }
                connector.removeDatasetFromCatalogue(datasetUriRef, DCATAPUriSchema.parseUriRef(datasetUriRef).recordUriRef(), res -> {
                    if (res.failed()) {
                        log.error("Remove catalogue entries", ar.cause());
                    }
                });
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService indexDataset(String datasetId, String catalogueId, Handler<AsyncResult<JsonObject>> handler) {
        String contentType = "application/n-triples";
        getDataset(datasetId, catalogueId, contentType, ar -> {
            if (ar.succeeded()) {
                DatasetHelper.create(datasetId, ar.result().getString("content"), contentType, null, catalogueId, dr -> {
                    if (dr.succeeded()) {
                        DatasetHelper helper = dr.result();
                        DatasetToIndexConverter datasetToIndexConverter = new DatasetToIndexConverter(config);
                        JsonObject indexObject = datasetToIndexConverter.convert2(helper);
                        indexService.addDatasetPut(indexObject, ir -> {
                            if (ir.failed()) {
                                handler.handle(Future.failedFuture(ir.cause()));
                            } else {
                                handler.handle(Future.succeededFuture());
                            }
                        });
                    } else {
                        handler.handle(Future.failedFuture(dr.cause()));
                    }
                });
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    public DatasetsService getDataUploadInformation(String datasetId, String catalogueId, String resultDataset, Handler<AsyncResult<JsonObject>> handler) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(getClass());
        DatasetHelper.create(resultDataset, Lang.NTRIPLES.getHeaderString(), dr -> {
            if (dr.succeeded()) {
                DatasetHelper helper = dr.result();
                JsonArray uploadResponse = dataUploadConnector.getResponse(helper);
                JsonObject result = new JsonObject();
                result.put("status", "success");
                result.put("distributions", uploadResponse);
                handler.handle(Future.succeededFuture(result));
            } else {
                handler.handle(Future.failedFuture(dr.cause()));
            }
        });
        return this;
    }

    private void translate(DatasetHelper helper) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(helper,getClass());
        translationService.initializeTranslationProcess(helper.stringify(Lang.TURTLE), helper.id(), helper.catalogueId(), ar -> {
            if (ar.succeeded()) {
                log.debug("Requesting a new translation for model.");
            } else if (ar.failed()) {
                log.error("Dataset could not submitted to translation service.");
                log.error(ar.cause().toString());
            }
        });
    }

    private void getHash(DatasetHelper helper, Handler<AsyncResult<JsonObject>> handler) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(helper,getClass());
        String query = "SELECT ?hash ?record WHERE {<" + helper.catalogueUriRef() + "> <" + DCAT.record + "> ?record. ?record <" + DCTerms.identifier + "> \"" + helper.id() + "\"; <" + SPDX.checksum + ">/<" + SPDX.checksumValue + "> ?hash}";
        connector.query(query, "application/sparql-results+json", ar -> {
            if (ar.succeeded()) {
                ResultSet set = ResultSetFactory.fromJSON(new ByteArrayInputStream(ar.result().body().getBytes()));
                if (set.hasNext()) {
                    QuerySolution solution = set.next();
                    RDFNode hash = solution.get("hash");
                    if (hash != null && hash.isLiteral()) {
                        log.debug("Hash available");
                        handler.handle(Future.succeededFuture(new JsonObject()
                                .put("success", true)
                                .put("hash", hash.asLiteral().toString())
                                .put("recordUriRef", solution.getResource("record").toString())));
                    } else {
                        log.debug("No old hash available");
                        handler.handle(Future.succeededFuture(new JsonObject().put("success", false)));
                    }
                } else {
                    log.debug("No old hash available");
                    handler.handle(Future.succeededFuture(new JsonObject().put("success", false)));
                }
            } else {
                log.error("Hash selection failed");
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    private void store(DatasetHelper helper, Handler<AsyncResult<JsonObject>> handler) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(helper,getClass());
        log.debug("Store dataset");
        connector.putGraph(helper.graphName(), helper.model(), ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                if (response.statusCode() == 200) {
                    handler.handle(Future.succeededFuture(new JsonObject().put("status", "updated").put(HttpHeaders.LOCATION, helper.uriRef())));
                } else if (response.statusCode() == 201) {
                    handler.handle(Future.succeededFuture(new JsonObject()
                            .put("status", "created")
                            .put("dataset", helper.stringify(Lang.NTRIPLES))
                            .put(HttpHeaders.LOCATION, helper.uriRef())));
                } else {
                    log.error("Store dataset: {}", response.statusMessage());
                    handler.handle(Future.failedFuture(response.statusMessage()));
                }
            } else {
                log.error("Store dataset", ar.cause());
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

    private void catalogue(DatasetHelper helper) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(helper,getClass());
        connector.addDatasetToCatalogue(helper.uriRef(), helper.recordUriRef(), helper.catalogueUriRef(), res -> {
            if (res.succeeded()) {
                HttpResponse<Buffer> response = res.result();
                if (response.statusCode() == 200) {
                    log.info("Catalogue entries created in {}", helper.catalogueUriRef());
                }
            } else {
                log.error("Adding catalogue entries to " + helper.catalogueUriRef() + ":", res.cause());
            }
        });
    }

    private void index(DatasetHelper helper) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(helper,getClass());
        DatasetToIndexConverter datasetToIndexConverter = new DatasetToIndexConverter(config);
        JsonObject indexMessage = datasetToIndexConverter.convert2(helper);
        indexService.addDatasetPut(indexMessage, ar -> {
            if (ar.failed()) {
                log.error("Indexing", ar.cause());
            }
        });
    }

    private void validate(DatasetHelper helper) {
        if (config.getJsonObject(Constants.ENV_HUB_VALIDATOR).getBoolean("enabled")) {
            vertx.eventBus().send(ValidationServiceVerticle.ADDRESS, helper.stringify(Lang.NTRIPLES));
        }
    }

}
