package io.piveau.hub.services.translation;

import io.piveau.hub.converters.DatasetToIndexConverter;
import io.piveau.hub.services.catalogues.CataloguesService;
import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.util.Constants;
import io.piveau.hub.util.DCATAPUriSchema;
import io.piveau.hub.util.DatasetHelper;
import io.piveau.hub.util.TSConnector;
import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.piveau.hub.util.rdf.EDP;
import io.piveau.hub.util.rdf.PropertyNotAvailableException;
import io.piveau.hub.util.rdf.PropertyParser;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DCTerms;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class TranslationServiceImpl implements TranslationService {



    private final PropertyParser propertyParser;
    private final DatasetsService datasetsService;
    private final CataloguesService cataloguesService;
    private final JsonObject config;
    private final TSConnector tsConnector;
    private final DatasetToIndexConverter converter;
    private final IndexService indexService;
    private final boolean isTranslationEnabled;

    private WebClient client;

    public TranslationServiceImpl(Vertx vertx, WebClient client, JsonObject config, TSConnector tsConnector,
                                  Handler<AsyncResult<TranslationService>> readyHandler) {
        PiveauLoggerFactory.getLogger(getClass()).debug("New instance of TranslationService created.");
        this.client = client;
        this.propertyParser = new PropertyParser();
        this.datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
        this.cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);
        this.config = config;
        this.tsConnector = tsConnector;
        this.converter = new DatasetToIndexConverter( config);
        this.indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS);
        this.isTranslationEnabled = this.config.getJsonObject(Constants.ENV_TRANSLATION_SERVICE).getBoolean("enable");
        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public TranslationService initializeTranslationProcess(String dataset, String datasetId, String catalogueId, Handler<AsyncResult<JsonObject>> asyncHandler) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(datasetId,catalogueId,getClass());

        if (isTranslationEnabled) {
            log.debug("Incoming translation request");
            DatasetHelper.create(datasetId, dataset, "text/turtle", null, catalogueId, ar -> {
                if (ar.succeeded()) {
                    DatasetHelper helper = ar.result();

                    addCatalogRecordDetailsBeforTranslation(helper);
                    // send updates for translations to store and index
                    sendTranslationToStore(helper);
                    sendTranslationToIndex(helper);

                    final Resource resource = helper.resource();
                    try {
                        final JsonObject requestBody = buildJsonFromResource(resource, helper);
                        sendRequestToTranslationServiceMiddleware(requestBody, asyncHandler);
                        asyncHandler.handle(Future.succeededFuture());
                    } catch (PropertyNotAvailableException e) {
                        log.error("Necessary properties not available.");
                        asyncHandler.handle(Future.failedFuture("Request could not sent."));
                    }

                } else {
                    asyncHandler.handle(Future.failedFuture(ar.cause()));
                }
            });
        } else {
            asyncHandler.handle(Future.succeededFuture());
        }
        return this;
    }

    @Override
    public TranslationService receiveTranslation(JsonObject translation, Handler<AsyncResult<JsonObject>> asyncHandler) {
        // Reading received informations
        PiveauLoggerFactory.getLogger(getClass()).debug("Incoming completed translation.");

        String resourceId = translation.getString("id");
        String datasetId = parseDatasetId(resourceId);
        String catalogueId = parseCatalogId(resourceId);

        // Build datasetUri from datasetId
        String originalLanguage = translation.getString("original_language");
        JsonObject translations = translation.getJsonObject("translation");

        // Get Model to add translations
        datasetsService.getDataset(datasetId, catalogueId,  "text/turtle", ar -> {
            PiveauLogger log = PiveauLoggerFactory.getLogger(datasetId,catalogueId,getClass());
            log = PiveauLoggerFactory.getLogger(datasetId,catalogueId,getClass());
            if (ar.succeeded()) {
                asyncHandler.handle(Future.succeededFuture(new JsonObject().put("status", "success")));
                DatasetHelper.create(datasetId, ar.result().getString("content"), "text/turtle", null, catalogueId, dr -> {
                    if (dr.succeeded()) {
                        try {
                            DatasetHelper helper = dr.result();
                            // Manipulating this model by adding translations
                            Model model = addTranslationsToModel(helper, translations, originalLanguage);
                            // Updating catalog record with translation informations
                            model = addCatalogRecordDetailsAfterTranslation(helper, originalLanguage);

                            // Write model back to store
                            sendTranslationToStore(helper);
                            sendTranslationToIndex(helper);
                        } catch (PropertyNotAvailableException e) {
                            e.printStackTrace();
                        }
                    } else {
                        asyncHandler.handle(Future.failedFuture(dr.cause()));
                    }
                });
            } else {
                log.error("Dataset not found.", ar.cause());
                asyncHandler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    private Model addTranslationsToModel(DatasetHelper helper, JsonObject translations, String originalLanguage) throws PropertyNotAvailableException {
        Model translationModel = removeOldTranslations(helper, originalLanguage);
        for (String language : translations.fieldNames()) {
            String languageTag = this.buildLanguageTag(originalLanguage, language);
            JsonObject attributes = translations.getJsonObject(language);
            for (String attribute : attributes.fieldNames()) {
                // for every attribute of every language
                Resource resource = translationModel.getResource(helper.uriRef());
                if (attribute.equals("title")) {
                    Literal titleLiteral = translationModel.createLiteral(attributes.getString(attribute), languageTag);
                    translationModel.add(resource, DCTerms.title, titleLiteral);
                } else if (attribute.equals("description")) {
                    Literal descriptionLiteral = translationModel.createLiteral(attributes.getString(attribute), languageTag);
                    translationModel.add(resource, DCTerms.description, descriptionLiteral);
                } else {
                    JsonArray distributions = this.propertyParser.getDcatDistribution(resource);
                    for (int distIndex = 0; distIndex < distributions.getList().size(); ++distIndex) {
                        JsonObject distribution = distributions.getJsonObject(distIndex);
                        String distributionId = distribution.getString("id");
                        String distributionUri = DCATAPUriSchema.applyFor(distributionId).distributionUriRef();
                        Resource distributionResource = translationModel.getResource(distributionUri);
                        if (attribute.endsWith("titl")) {
                            Literal distributionTitleLiteral = translationModel.createLiteral(attributes.getString(attribute), languageTag);
                            translationModel.add(distributionResource, DCTerms.title, distributionTitleLiteral);
                        } else if (attribute.endsWith("desc")) {
                            Literal distributionDescriptionLiteral = translationModel.createLiteral(attributes.getString(attribute), languageTag);
                            translationModel.add(distributionResource, DCTerms.description, distributionDescriptionLiteral);
                        }
                    }
                }
            }
        }
        return translationModel;
    }

    private Model removeOldTranslations(DatasetHelper helper, String originalLanguage)
            throws PropertyNotAvailableException {
        Model model = helper.model();
        Resource resource = helper.resource();


        String originalTitle = this.getOriginalText(propertyParser.getDctTitle(resource), originalLanguage);
        String originalDescription = this.getOriginalText(propertyParser.getDctDescription(resource), originalLanguage);
        Literal titleLiteral = model.createLiteral(originalTitle, originalLanguage);
        Literal descriptionLiteral = model.createLiteral(originalDescription, originalLanguage);

        model.removeAll(resource, DCTerms.title, null);
        model.removeAll(resource, DCTerms.description, null);
        model.add(resource, DCTerms.title, titleLiteral);
        model.add(resource, DCTerms.description, descriptionLiteral);

        JsonArray distributions = this.propertyParser.getDcatDistribution(resource);
        for (int distIndex = 0; distIndex < distributions.getList().size(); ++distIndex) {
            JsonObject distribution = distributions.getJsonObject(distIndex);
            String distributionId = distribution.getString("id");
            String distributionUri = DCATAPUriSchema.applyFor(distributionId).distributionUriRef();
            Resource distributionResource = model.getResource(distributionUri);

            String originalDistributionTitle = this.getOriginalText(propertyParser.getDctTitle(distributionResource), originalLanguage);
            String originalDistributionDescription = this.getOriginalText(propertyParser.getDctDescription(distributionResource), originalLanguage);
            Literal originalDistributionTitleLiteral = model.createLiteral(originalDistributionTitle, originalLanguage);
            Literal originalDistributionDescriptionLiteral = model.createLiteral(originalDistributionDescription, originalLanguage);

            model.removeAll(distributionResource, DCTerms.title, null);
            model.removeAll(distributionResource, DCTerms.description, null);
            model.add(distributionResource, DCTerms.title, originalDistributionTitleLiteral);
            model.add(distributionResource, DCTerms.description, originalDistributionDescriptionLiteral);
        }
        return model;
    }

    private Model addCatalogRecordDetailsBeforTranslation(DatasetHelper helper) {

        Resource record = helper.recordResource();

        Model model = helper.model();

        model.removeAll(record, EDP.edpTranslationReceived, null);
        model.removeAll(record, EDP.edpTranslationIssued, null);
        model.add(record, EDP.edpTranslationIssued, ZonedDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_DATE_TIME), XSDDatatype.XSDdateTime);
        model.removeAll(record, EDP.edpTranslationStatus, EDP.edpTranslationCompleted);
        model.add(record, EDP.edpTranslationStatus, EDP.edpTranslationInProcess);

        return model;
    }

    private Model addCatalogRecordDetailsAfterTranslation(DatasetHelper helper, String originalLanguage) {
        Resource record = helper.recordResource();

        Model model = helper.model();
        model.removeAll(record, EDP.edpTranslationReceived, null);
        model.add(record, EDP.edpTranslationReceived, ZonedDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_DATE_TIME), XSDDatatype.XSDdateTime);
        model.removeAll(record, EDP.edpOriginalLanguage, null);
        model.add(record, EDP.edpOriginalLanguage, this.generateLanguageUri(originalLanguage));
        model.removeAll(record, EDP.edpTranslationStatus, EDP.edpTranslationInProcess);
        model.add(record, EDP.edpTranslationStatus, EDP.edpTranslationCompleted);

        return model;
    }

    private void sendTranslationToStore(DatasetHelper helper) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(helper,getClass());
        this.tsConnector.putGraph(helper.graphName(), helper.model(), ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                if (response.statusCode() == 200) {
                    log.debug("Dataset updated with translation information in store successful.");
                } else if (response.statusCode() == 201) {
                    log.debug("Translation created successful.");
                } else {
                    log.error("Put dataset: {}", response.statusMessage());
                }
            } else {
                log.error("Put dataset", ar.cause());
            }
        });
    }

    private void sendTranslationToIndex(DatasetHelper helper) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(helper,getClass());
        indexService.addDatasetPut(converter.convert2(helper), ar -> {
            if (ar.succeeded()) {
                log.debug("Successfully send to Index Service");
            } else {
                log.error("Dataset could not send to IndexService", ar.cause());
            }
        });
    }

    private void sendRequestToTranslationServiceMiddleware(JsonObject requestBody, Handler<AsyncResult<JsonObject>> asyncHandler) {
        PiveauLogger log = PiveauLoggerFactory.getLogger(getClass());
        String translationServiceUrl = this.config.getJsonObject(Constants.ENV_TRANSLATION_SERVICE).getString("translation_service_url");
        client.postAbs(translationServiceUrl)
                .putHeader("Content-Type", "application/json")
                .sendJsonObject(requestBody, ar -> {
                    if (ar.succeeded()) {
                        log.debug("Translation request sent successfully.");
                        log.debug(ar.result().bodyAsString());
                        asyncHandler.handle(Future.succeededFuture());
                    } else if (ar.failed()) {
                        log.error("Translation request failed.", ar.cause());
                        asyncHandler.handle(Future.failedFuture("Request to Translation Service Middleware failed."));
                    }
                });
    }

    private JsonObject buildJsonFromResource(Resource resource, DatasetHelper helper) throws PropertyNotAvailableException {
        JsonObject requestBody = new JsonObject();
        PiveauLogger log = PiveauLoggerFactory.getLogger(helper,getClass());

        // Adding original language
        final String originalLanguage = getOriginalLanguage(resource);
        requestBody.put("original_language", originalLanguage);
        log.debug("Identified original language: " + originalLanguage);

        // Adding languages for translation request
        final Set set = getTranslationLanguages(originalLanguage);
        requestBody.put("languages", new JsonArray(new ArrayList<String>(set)));

        // Adding callback parameters
        requestBody.put("callback", getCallbackParameters(helper.id(), helper.catalogueId()));

        // Adding data dict to translate
        requestBody.put("data_dict", getDataDict(resource, originalLanguage));

        return requestBody;
    }

    private JsonObject getDataDict(Resource resource, String originalLanguage)
            throws PropertyNotAvailableException {
        JsonObject dataDict = new JsonObject();
        String title = this.propertyParser.getDctTitle(resource).getString(originalLanguage);
        String description = this.propertyParser.getDctDescription(resource).getString(originalLanguage);
        JsonArray distributions = this.propertyParser.getDcatDistribution(resource);
        for (int distIndex = 0; distIndex < distributions.getList().size(); ++distIndex) {
            JsonObject distribution = distributions.getJsonObject(distIndex);
            String distTitle = distribution.getJsonObject("title").getString(originalLanguage);
            String distDecription = distribution.getJsonObject("description").getString(originalLanguage);
            String distId = distribution.getString("id");
            dataDict.put(distId + "titl", distTitle);
            dataDict.put(distId + "desc", distDecription);
        }
        dataDict.put("title", title);
        dataDict.put("description", description);
        return dataDict;
    }

    private JsonObject getCallbackParameters(String datasetId, String catalogueId) {
        JsonObject callbackParameters = new JsonObject();
        String callbackUrl = this.config.getJsonObject(Constants.ENV_TRANSLATION_SERVICE).getString("callback_url");
        String callbackAuth = this.config.getString(Constants.ENV_HUB_API_KEY);

        callbackParameters.put("url", callbackUrl);
        callbackParameters.put("method", "POST");
        JsonObject payload = new JsonObject();

        payload.put("id", this.buildDatasetCatalogId(datasetId, catalogueId));

        callbackParameters.put("payload", payload);
        JsonObject headers = new JsonObject();
        headers.put("Authorization", callbackAuth);
        callbackParameters.put("headers", headers);
        return callbackParameters;
    }

    private String getOriginalLanguage(Resource resource) throws PropertyNotAvailableException {
        JsonObject title = this.propertyParser.getDctTitle(resource);
        Set keys = title.fieldNames();
        if (keys.size() == 1) {
            // take language which is once given
            return (String) keys.toArray()[0];
        } else if (title.containsKey("en")) {
            // take 'en' if possible
            return "en";
        } else {
            // take one language from list of languages
            while (keys.iterator().hasNext()) {
                String key = (String) keys.iterator().next();
                if (key.length() == 2) {
                    return key;
                }
            }
            return "en";
        }
    }

    private String getOriginalText(JsonObject resourceObject, String originalLanguage) {
        String originalText = null;
        for (String key : resourceObject.fieldNames()) {
            if (key.equals(originalLanguage) || (key.contains(originalLanguage) && !key.contains("mtec"))) {
                originalText = resourceObject.getString(key);
            }
        }
        return originalText;
    }

    private Set getTranslationLanguages(String originalLanguage) {
        Set translationLanguages = getAcceptedLanguages();
        translationLanguages.remove(originalLanguage);
        return translationLanguages;
    }

    private Set getAcceptedLanguages() {
        List acceptedLanguages = this.config.getJsonObject(Constants.ENV_TRANSLATION_SERVICE).getJsonArray("accepted_languages").getList();
        return new HashSet<String>(acceptedLanguages);
    }

    private String buildLanguageTag(String originalLanguage, String targetLanguage) {
        if (targetLanguage.equals("nb"))
            targetLanguage = "no";
        return targetLanguage + "-t-" + originalLanguage + "-t0-mtec";
    }

    private String buildDatasetCatalogId(String datasetId, String catalogId) {
        return datasetId + "+" + catalogId;
    }

    private String parseDatasetId(String responseId) {
        return responseId.substring(0, responseId.indexOf("+"));
    }

    private String parseCatalogId(String responseId) {
        return responseId.substring(responseId.indexOf("+") + 1);
    }

    private Resource generateLanguageUri(String languageCode) {
        String string_uri;
        switch (languageCode) {
            case "en": string_uri = "http://publications.europa.eu/resource/authority/language/ENG"; break;
            case "bg": string_uri = "http://publications.europa.eu/resource/authority/language/BUL"; break;
            case "hr": string_uri = "http://publications.europa.eu/resource/authority/language/HRV"; break;
            case "cs": string_uri = "http://publications.europa.eu/resource/authority/language/CES"; break;
            case "da": string_uri = "http://publications.europa.eu/resource/authority/language/DAN"; break;
            case "nl": string_uri = "http://publications.europa.eu/resource/authority/language/NLD"; break;
            case "et": string_uri = "http://publications.europa.eu/resource/authority/language/EST"; break;
            case "fi": string_uri = "http://publications.europa.eu/resource/authority/language/FIN"; break;
            case "fr": string_uri = "http://publications.europa.eu/resource/authority/language/FRA"; break;
            case "el": string_uri = "http://publications.europa.eu/resource/authority/language/ELL"; break;
            case "hu": string_uri = "http://publications.europa.eu/resource/authority/language/HUN"; break;
            case "ga": string_uri = "http://publications.europa.eu/resource/authority/language/GLE"; break;
            case "it": string_uri = "http://publications.europa.eu/resource/authority/language/ITA"; break;
            case "lv": string_uri = "http://publications.europa.eu/resource/authority/language/LAV"; break;
            case "lt": string_uri = "http://publications.europa.eu/resource/authority/language/LIT"; break;
            case "mt": string_uri = "http://publications.europa.eu/resource/authority/language/MLT"; break;
            case "pl": string_uri = "http://publications.europa.eu/resource/authority/language/POL"; break;
            case "pt": string_uri = "http://publications.europa.eu/resource/authority/language/POR"; break;
            case "ro": string_uri = "http://publications.europa.eu/resource/authority/language/RON"; break;
            case "sk": string_uri = "http://publications.europa.eu/resource/authority/language/SLK"; break;
            case "sl": string_uri = "http://publications.europa.eu/resource/authority/language/SLV"; break;
            case "es": string_uri = "http://publications.europa.eu/resource/authority/language/SPA"; break;
            case "sv": string_uri = "http://publications.europa.eu/resource/authority/language/SWE"; break;
            case "nb": string_uri = "http://publications.europa.eu/resource/authority/language/NOB"; break;
            case "de": string_uri = "http://publications.europa.eu/resource/authority/language/DEU"; break;
            default: string_uri = "http://publications.europa.eu/resource/authority/language/ENG"; break;
        }
        return ResourceFactory.createResource(string_uri);
    }

}
