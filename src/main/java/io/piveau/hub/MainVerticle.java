package io.piveau.hub;

import io.piveau.hub.handler.*;
import io.piveau.hub.services.catalogues.CataloguesService;
import io.piveau.hub.services.catalogues.CataloguesServiceVerticle;
import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.datasets.DatasetsServiceVerticle;
import io.piveau.hub.services.distributions.DistributionsService;
import io.piveau.hub.services.distributions.DistributionsServiceVerticle;
import io.piveau.hub.services.index.IndexServiceVerticle;
import io.piveau.hub.services.translation.TranslationService;
import io.piveau.hub.services.translation.TranslationServiceVerticle;
import io.piveau.hub.services.validation.ValidationServiceVerticle;
import io.piveau.hub.util.Constants;
import io.piveau.hub.util.DCATAPUriSchema;
import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.piveau.hub.util.rdf.VocabularyManager;
import io.piveau.hub.verticles.ShellVerticle;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.RouterFactoryOptions;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.StaticHandler;

import java.util.Arrays;

/**
 * This is the main entry point of the application
 */
public class MainVerticle extends AbstractVerticle {



    private CatalogueHandler catalogueHandler;
    private ApiKeyHandler apiKeyHandler;

    private DatasetHandler datasetHandler;
    private DistributionHandler distributionHandler;

    private TranslationServiceHandler translationServiceHandler;

    /**
     * Composes all function for starting the Main Verticle
     */
    @Override
    public void start(Future<Void> startFuture) {
        PiveauLoggerFactory.getLogger(getClass()).info("Starting hub-core...");
        loadConfig()
                .compose(this::bootstrapVerticles)
                .compose(this::startServer)
                .setHandler(startFuture.completer());
    }

    /**
     * Starts the HTTP Server based on the OpenAPI Specification
     * ToDo Check the status of depended services here (e.g. search, triplestore, validation)
     *
     * @return
     */
    private Future<Void> startServer(JsonObject config) {
        PiveauLogger.setBaseUri(config.getString(Constants.ENV_HUB_BASE_URI, "https://io.piveau/"));
        Future<Void> future = Future.future();
        PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());
        VocabularyManager.init(config, vertx, ar -> {});

        DCATAPUriSchema.config(new JsonObject().put("baseUri", config.getString(Constants.ENV_HUB_BASE_URI, "https://io.piveau/")));

        Integer port = config.getInteger(Constants.ENV_HUB_SERVICE_PORT);
        String apiKey = config.getString(Constants.ENV_HUB_API_KEY);
        String greeting = config.getString("greeting");

        if (apiKey == null || apiKey.isEmpty()) {
            future.fail(Constants.ENV_HUB_API_KEY + " is missing in the config file. Please specify it.");
            return future;
        }

        // Loads the OpenAPI specification and creates the routes
        OpenAPI3RouterFactory.create(vertx, "webroot/openapi.yaml", handler -> {
            if (handler.succeeded()) {
                OpenAPI3RouterFactory routerFactory = handler.result();
                RouterFactoryOptions options = new RouterFactoryOptions().setMountNotImplementedHandler(true);
                routerFactory.setOptions(options);

                apiKeyHandler = new ApiKeyHandler(apiKey);
                routerFactory.addSecurityHandler("ApiKeyAuth", apiKeyHandler::checkApiKey);

                routerFactory.addHandlerByOperationId("listDatasets", datasetHandler::handleListDatasets);
                //routerFactory.addHandlerByOperationId("postDataset", datasetHandler::handlePostDataset);
                routerFactory.addHandlerByOperationId("putDataset", datasetHandler::handlePutDataset);
                routerFactory.addHandlerByOperationId("getDataset", datasetHandler::handleGetDataset);
                routerFactory.addHandlerByOperationId("deleteDataset", datasetHandler::handleDeleteDataset);
                routerFactory.addHandlerByOperationId("indexDataset", datasetHandler::handleIndexDataset);

                routerFactory.addHandlerByOperationId("getRecord", datasetHandler::handleGetRecord);

                routerFactory.addHandlerByOperationId("listCatalogues", catalogueHandler::handleListCatalogues);
                //routerFactory.addHandlerByOperationId("postCatalogue", catalogueHandler::handlePostCatalog);
                routerFactory.addHandlerByOperationId("putCatalogue", catalogueHandler::handlePutCatalogue);
                routerFactory.addHandlerByOperationId("getCatalogue", catalogueHandler::handleGetCatalogue);
                routerFactory.addHandlerByOperationId("deleteCatalogue", catalogueHandler::handleDeleteCatalogue);

                routerFactory.addHandlerByOperationId("getDistribution", distributionHandler::handleGetDistribution);

                routerFactory.addHandlerByOperationId("postTranslationRequest", translationServiceHandler::handlePostTranslationRequest);
                routerFactory.addHandlerByOperationId("postTranslation", translationServiceHandler::handlePostTranslation);


                Router router = routerFactory.getRouter();
                router.route("/*").handler(StaticHandler.create());
                router.route("/info").handler(context -> healthHandler(context, greeting));

                HttpServer server = vertx.createHttpServer(new HttpServerOptions().setPort(port));
                server.requestHandler(router).listen();

                LOGGER.info("Successfully launched server on port [{}]", port);

                future.complete();
            } else {
                // Something went wrong during router factory initialization
                LOGGER.error("Failed to start server at [{}]: {}", port, handler.cause());
                future.fail(handler.cause());
            }
        });


        return future;
    }

    /**
     * Loads the configuration file
     *
     * @return Configuration Object
     */
    private Future<JsonObject> loadConfig() {
        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add(Constants.ENV_HUB_SERVICE_PORT)
                        .add(Constants.ENV_HUB_API_KEY)
                        .add(Constants.ENV_HUB_BASE_URI)
                        .add(Constants.ENV_HUB_VALIDATOR)
                        .add(Constants.ENV_HUB_TRIPLESTORE_CONFIG)
                        .add(Constants.ENV_HUB_SEARCH_SERVICE)
                        .add(Constants.ENV_TRANSLATION_SERVICE)
                        .add(Constants.ENV_DATA_UPLOAD)
                ));

        ConfigStoreOptions fileStoreOptions = new ConfigStoreOptions()
                .setType("file")
                .setConfig(new JsonObject().put("path", "conf/config.json")); // ToDo What is right here?

        ConfigRetriever retriever = ConfigRetriever
                .create(vertx, new ConfigRetrieverOptions()
                        .addStore(fileStoreOptions)
                        .addStore(envStoreOptions));

        return ConfigRetriever.getConfigAsFuture(retriever);
    }

    public MainVerticle() {
        super();
    }

    /**
     * Bootstraps all Verticles
     *
     * @return future
     */
    private Future<JsonObject> bootstrapVerticles(JsonObject config) {
        PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());
        LOGGER.info(config.encodePrettily());

        Future<JsonObject> future = Future.future();
        DeploymentOptions options = new DeploymentOptions().setConfig(config).setWorker(true);

        DeploymentOptions shellOptions = new DeploymentOptions().setConfig(config);
        Future<String> shellFuture = Future.future();
        vertx.deployVerticle(ShellVerticle.class.getName(), shellOptions, shellFuture);

        Future<String> indexFuture = Future.future();
        vertx.deployVerticle(IndexServiceVerticle.class.getName(), options, indexFuture);


        Future<String> datasetsFuture = Future.future();
        vertx.deployVerticle(DatasetsServiceVerticle.class.getName(), new DeploymentOptions().setConfig(config).setWorker(true).setInstances(4), datasetsFuture);

        Future<String> distributionsFuture = Future.future();
        vertx.deployVerticle(DistributionsServiceVerticle.class.getName(), options, distributionsFuture);

        Future<String> catalogsFuture = Future.future();
        vertx.deployVerticle(CataloguesServiceVerticle.class.getName(), options, catalogsFuture);

        Future<String> translationSeviceFuture = Future.future();
        vertx.deployVerticle(TranslationServiceVerticle.class.getName(), options, translationSeviceFuture);

        Future<String> validationSeviceFuture = Future.future();
        vertx.deployVerticle(ValidationServiceVerticle.class.getName(), new DeploymentOptions().setConfig(config).setWorker(true).setInstances(4), validationSeviceFuture);

        CompositeFuture.all(Arrays.asList(indexFuture, datasetsFuture, distributionsFuture,
                catalogsFuture, translationSeviceFuture, validationSeviceFuture)).setHandler(ar -> {
            if (ar.succeeded()) {
                datasetHandler = new DatasetHandler(vertx, DatasetsService.SERVICE_ADDRESS);
                distributionHandler = new DistributionHandler(vertx, DistributionsService.SERVICE_ADDRESS);
                catalogueHandler = new CatalogueHandler(vertx, CataloguesService.SERVICE_ADDRESS);
                translationServiceHandler = new TranslationServiceHandler(vertx, TranslationService.SERVICE_ADDRESS);
                future.complete(config);
            } else {
                future.fail(ar.cause());
            }
        });

        return future;
    }

    /**
     * Creates the health and info endpoint
     *
     * @param context
     */
    private void healthHandler(RoutingContext context, String greeting) {
        JsonObject response = new JsonObject();
        response.put("service", "hub-core");
        response.put("message", greeting);
        response.put("version", "0.0.1");
        response.put("status", "ok");
        context.response().setStatusCode(200);
        context.response().putHeader("Content-Type", "application/json");
        context.response().end(response.encode());
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

}
