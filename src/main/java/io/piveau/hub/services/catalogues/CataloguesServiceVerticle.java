package io.piveau.hub.services.catalogues;

import io.piveau.hub.util.Constants;
import io.piveau.hub.util.TSConnector;
import io.piveau.hub.util.ValidationConnector;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.serviceproxy.ServiceBinder;

public class CataloguesServiceVerticle extends AbstractVerticle {


    @Override
    public void start(Future<Void> startFuture) {
        WebClient client = WebClient.create(vertx);

        JsonObject conf = config().getJsonObject(Constants.ENV_HUB_TRIPLESTORE_CONFIG);
        JsonObject validatorConfig = config().getJsonObject(Constants.ENV_HUB_VALIDATOR);

        CircuitBreaker breaker = CircuitBreaker.create("virtuoso-breaker", vertx, new CircuitBreakerOptions().setMaxRetries(5))
                .retryPolicy(count -> count * 1000L);

        TSConnector connector = TSConnector.create(client, breaker, conf);
        ValidationConnector validationConnector = new ValidationConnector(client, validatorConfig.getString("url"));


        CataloguesService.create(connector, validationConnector, config(), vertx, ready -> {
            if (ready.succeeded()) {
                new ServiceBinder(vertx).setAddress(CataloguesService.SERVICE_ADDRESS).register(CataloguesService.class, ready.result());
                startFuture.complete();
            } else {
                startFuture.fail(ready.cause());
            }
        });
    }
}
