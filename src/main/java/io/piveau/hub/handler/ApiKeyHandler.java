package io.piveau.hub.handler;

import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handler for the API-Key. Used for all routes defined with ApiKeyAuth in the OpenAPI specs
 */
public class ApiKeyHandler {


    private final String apiKey;


    public ApiKeyHandler(String api_key) {
        this.apiKey = api_key;
    }

    public void checkApiKey(RoutingContext context) {

        String authorization = context.request().getHeader(HttpHeaders.AUTHORIZATION);

        if(this.apiKey.isEmpty()) {
            JsonObject response = new JsonObject();
            context.response().putHeader("Content-Type", "application/json");
            context.response().setStatusCode(500);
            response.put("status", "error");
            response.put("cause", "Api-Key is not specified");
        } else if(authorization == null) {
            JsonObject response = new JsonObject();
            context.response().putHeader("Content-Type", "application/json");
            context.response().setStatusCode(401);
            response.put("status", "error");
            response.put("cause", "Header field Authorization is missing");
            context.response().end(response.toString());
        } else if (!authorization.equals(this.apiKey)) {
            JsonObject response = new JsonObject();
            context.response().putHeader("Content-Type", "application/json");
            context.response().setStatusCode(401);
            response.put("status", "error");
            response.put("cause", "Incorrect Api-Key");
            context.response().end(response.toString());
        } else {
            context.next();
        }
    }
}
