package io.piveau.hub.converters;

import io.piveau.hub.util.DCATAPUriSchema;
import io.piveau.hub.util.DatasetHelper;
import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.piveau.hub.util.rdf.*;
import io.piveau.utils.JenaUtils;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.file.FileSystem;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Converts a Dataset Jena Model to JSON for the Search Index
 * ToDo Just a first version, should be more generic
 */
public class DatasetToIndexConverter {



//    private static DatasetToIndexConverter instance;

    private DateTimeUtil dateTimeUtil = new DateTimeUtil();
    private JsonObject config;
    private PropertyParser propertyParser;
    public Model euVoc;

//    private DatasetToIndexConverter(){}
//
//    public static DatasetToIndexConverter init(RDFUtil rdfUtil, JsonObject config) {
//        if (DatasetToIndexConverter.instance == null) {
//            DatasetToIndexConverter.instance = new DatasetToIndexConverter(rdfUtil,config);
//        }
//        return DatasetToIndexConverter.instance;
//    }
//
//    public static DatasetToIndexConverter getInstance() {
//        if (DatasetToIndexConverter.instance == null) {
//            LOGGER.error("DatasetToIndexConverter was never initiated");
//        }
//        return DatasetToIndexConverter.instance;
//    }

    public DatasetToIndexConverter(JsonObject config) {
        this.euVoc =  ModelFactory.createDefaultModel();
        this.config = config;
        this.propertyParser = new PropertyParser();
    }

    @Deprecated
    public Future<Void> loadVocabulary(Vertx vertx) {
        Future<Void> future = Future.future();
        if(config.getBoolean("PIVEAU_HUB_LOAD_VOCABULARY", true)) {
            PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(getClass());
            LOGGER.info("Init EuroVoc");
            FileSystem fileSystem = vertx.fileSystem();
            fileSystem.readFile("vocabularies/languages-skos.rdf", bufferAsyncResult -> {
                if (bufferAsyncResult.succeeded()) {
                    vertx.executeBlocking(handler -> {
                        RDFDataMgr.read(euVoc, new ByteArrayInputStream(bufferAsyncResult.result().getBytes()), Lang.RDFXML);
                        LOGGER.info("Created EuroVcc Model");
                        handler.complete();
                    }, res -> {
                        if (res.succeeded()) {
                            LOGGER.info("Loaded EuroVoc Files");
                            future.complete();
                        } else {
                            future.fail(res.cause());
                        }
                    });
                } else {
                    future.failed();
                }
            });
        } else {
            future.complete();
        }
        return future;
    }

    public JsonObject convert2(DatasetHelper helper) {
        PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(helper,getClass());
        JsonObject result = new JsonObject();

        JsonObject translationMeta = new JsonObject();

        Resource res = helper.resource();
        String normalizedId = DCATAPUriSchema.parseUriRef(res.getURI()).id();
        result.put("id", normalizedId);
        result.put("idName", normalizedId);

        String recordUri = helper.recordUriRef();
        Resource recordRes = helper.recordResource();

        JsonArray messages =  new JsonArray();


        try {
            JsonObject description = propertyParser.getMTECDctDescription(res);
            result.put("description", description.getJsonObject("payload"));
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonObject title = propertyParser.getMTECDctTitle(res);
            result.put("title", title.getJsonObject("payload"));
            JsonObject transInfo = propertyParser.getEdpTransInfo(recordRes);

            if(!title.getJsonObject("meta").isEmpty() && !transInfo.isEmpty()) {
                translationMeta.put("details", title.getJsonObject("meta"));

                try {
                    JsonArray fullAvailable = new JsonArray();
                    for (String language :  translationMeta.getJsonObject("details").fieldNames()) {
                        // Todo This is not 100% correct
                        fullAvailable.add(language);
                        JsonObject langObject =  translationMeta.getJsonObject("details").getJsonObject(language);
                        if(langObject.getBoolean("machine_translated")) {
                            langObject.mergeIn(transInfo);
                            langObject.remove("status");
                        }
                    }
                    translationMeta.put("status", transInfo.getString("status"));
                    translationMeta.put("full_available_languages", fullAvailable);

                } catch (RuntimeException e) {
                    if(e.getMessage() != null) {
                        messages.add(e.getMessage());
                    }
                }
            }
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

//        try {
//            JsonObject title = propertyParser.getDctTitle(res);
//            result.put("title", title);
//        } catch (PropertyNotAvailableException e) {
//            LOGGER.debug(e.getMessage());
//        }


//        try {
//            JsonObject description = propertyParser.getDctDescription(res);
//            result.put("description", description);
//        } catch (PropertyNotAvailableException e) {
//            LOGGER.debug(e.getMessage());
//        }

        try {
            JsonArray categories = propertyParser.getDcatTheme(res);
            result.put("categories", categories);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonArray languages = propertyParser.getDctLanguage(res);
            result.put("languages", languages);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonArray contactPoint = propertyParser.getDcatContactPoint(res);
            result.put("contact_points", contactPoint);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonArray distribution = propertyParser.getDcatDistribution(res);
            result.put("distributions", distribution);
        } catch (PropertyNotAvailableException e) {
            result.put("distributions", new JsonArray());
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonArray keywords = propertyParser.getDcatKeyword(res);
            result.put("keywords", keywords);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonObject spatial = propertyParser.getDctSpatialForDataset(res);
            result.put("spatial", spatial);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            String accessRight = propertyParser.getDctAccssRights(res);
            result.put("access_right", accessRight);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonArray conformsTo = propertyParser.getDctConformsTo(res);
            result.put("conforms_to", conformsTo);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonArray documentation = propertyParser.getFoafPage(res);
            result.put("documentations", documentation);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            String releaseDate = propertyParser.getDctIssued(res);
            result.put("release_date", releaseDate);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            String modDate = propertyParser.getDctModified(res);
            result.put("modification_date", modDate);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonObject publisher = propertyParser.getDctPublisher(res);
            result.put("publisher", publisher);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        try {
            JsonArray landingPage = propertyParser.getDcatLandingPage(res);
            result.put("landing_page", landingPage);
        } catch (PropertyNotAvailableException e) {
            if(e.getMessage() != null) {
                messages.add(e.getMessage());
            }
        }

        result.put("catalog", new JsonObject().put("id", helper.catalogueId()));

        if(translationMeta.getJsonObject("details") != null && !translationMeta.getJsonObject("details").isEmpty()) {
            result.put("translation_meta", translationMeta);
        }

        LOGGER.debug(messages.toString());
        return result;

    }
/*
    public JsonObject convert2(DatasetHelper helper) {
        return convert2(helper.model(), helper.uriRef(), helper.id(), helper.catalogueId());
    }
*/

    @Deprecated
    public JsonObject convert(Model model, String id) {

        JsonObject result = new JsonObject();
        result.put("id", id);

        Resource res;
        ResIterator resIterator = model.listResourcesWithProperty(DCATAP.rdfType, DCATAP.DcatDataset);
        if(resIterator.hasNext()) {
            res = model.listResourcesWithProperty(DCATAP.rdfType, DCATAP.DcatDataset).nextResource();
        } else {
            return result;
        }

        getProperty(res, DCATAP.dctTitle, statement -> {
            result.put("title", new JsonObject().put("en", statement.getString()));
        });

        getProperty(res, DCATAP.dctDescription, statement -> {
            result.put("description", new JsonObject().put("en", statement.getString()));
        });

        getProperties(res, DCATAP.dcatTheme, stmtIterator -> {
            JsonArray themes = new JsonArray();
            while(stmtIterator.hasNext()) {
                String themeURI = stmtIterator.nextStatement().getObject().toString();
                if(DCATAP.themeMapping().containsKey(themeURI)) {
                    themes.add(DCATAP.themeMapping().get(themeURI));
                }
            }
            if(!themes.isEmpty()) {
                result.put("categories", themes);
            }
        });

        getProperty(res, DCATAP.dctIssued, statement -> {
            String date = dateTimeUtil.parse(statement.getString());
            if(date != null) {
                result.put("release_date", date);
            }
        });

        getProperty(res, DCATAP.dctModified, statement -> {
            String date = dateTimeUtil.parse(statement.getString());
            if(date != null) {
                result.put("modification_date", date);
            }
        });

        JsonArray contactPoints = getListOfNestedProperties(res, DCATAP.dcatContactPoint, new HashMap<Property, String>(){
            {
                put(DCATAP.vcardHasEmail, "email");
                put(DCATAP.vcardFn, "name");
        }
        });
        if(!contactPoints.isEmpty()) {
            result.put("contact_points", contactPoints);
        }

        getProperty(res, DCATAP.dctPublisher, statement -> {
            JsonObject publisher = getNestedProperties(statement.getResource(), new HashMap<Property, String>(){
                {
                    put(DCATAP.foafHomepage, "email");
                    put(DCATAP.foafName, "name");
                }
            });
            if(!publisher.isEmpty()) {
                result.put("publisher", publisher);
            }
        });

        getProperties(res, DCATAP.dcatKeyword, stmtIterator -> {
            JsonArray keywords = new JsonArray();
            while(stmtIterator.hasNext()) {
                String keyword = stmtIterator.nextStatement().getString();
                keywords.add(new JsonObject()
                    .put("id", JenaUtils.normalize(keyword))
                    .put("title", keyword));
            }
            if(!keywords.isEmpty()) {
                result.put("keywords", keywords);
            }
        });

        getProperty(res, DCATAP.dctLanguage, statement -> {
            if(statement.getObject().isResource()) {
                String lang = resolveLanguage(statement.getObject().asResource().getURI());
                if(lang != null) {
                    result.put("languages", new JsonArray().add(lang));
                }

            }
        });

        getProperties(res, DCATAP.dcatDistribution, stmtIterator -> {
            JsonArray distributions = new JsonArray();
            while(stmtIterator.hasNext()) {
                JsonObject distribution = getDistribution(stmtIterator.nextStatement().getResource());
                distributions.add(distribution);
            }
            if(!distributions.isEmpty()) {
                result.put("distributions", distributions);
            }
        });

//        LOGGER.info(result.encodePrettily());

        return result;
    }

    @Deprecated
    private JsonObject getDistribution(Resource resource) {
        JsonObject result = new JsonObject();

        getDistFormat(resource, success -> {
            result.put("format", success);
        });


        getProperty(resource, DCATAP.dcatAccessURL, statement -> {
            result.put("access_url", statement.getObject().toString());
        });

        getProperty(resource, DCATAP.dctLicense, statement -> {
            result.put("licence", new JsonObject()
                .put("id", JenaUtils.normalize(statement.getObject().toString()))
                .put("title", JenaUtils.normalize(statement.getObject().toString()))
                .put("resource", statement.getObject().toString()));
        });

        getProperty(resource, DCATAP.dctTitle, statement -> {
            result.put("title",  new JsonObject().put("en", statement.getString()));
        });

        getProperty(resource, DCATAP.dctDescription, statement -> {
            result.put("description",  new JsonObject().put("en", statement.getString()));
        });

        return result;
    }

    @Deprecated
    private void getProperty(Resource res, Property prop, Handler<Statement> handler) {
        if(res.getProperty(prop) != null) {
            handler.handle(res.getProperty(prop));
        }
    }

    @Deprecated
    private void getProperties(Resource res, Property prop, Handler<StmtIterator> handler) {
        if(res.listProperties(prop) != null) {
            handler.handle(res.listProperties(prop));
        }
    }

    @Deprecated
    private JsonObject getNestedProperties(Resource res, Map<Property, String> props) {
        JsonObject result = new JsonObject();
        props.forEach((k,v) -> {
            getProperty(res, k, statement -> {
                result.put(v, determineSingeLiteral(res, k));
            });
        });
        return result;
    }

    @Deprecated
    private JsonArray getListOfNestedProperties(Resource res, Property prop, Map<Property, String> subProps) {
        JsonArray result = new JsonArray();
        getProperties(res, prop, stmtIterator -> {
            while(stmtIterator.hasNext()) {
                JsonObject subProperty = getNestedProperties(stmtIterator.nextStatement().getResource(), subProps);
                if(!subProperty.isEmpty()) {
                    result.add(subProperty);
                }
            }
        });
        return result;
    }

    /**
     * Tries to return a literal for a given property.
     *
     * ToDo Still very ugly...
     * @param resource
     * @param property
     * @return
     */
    @Deprecated
    private String determineSingeLiteral(Resource resource, Property property) {
        JsonObject result = new JsonObject();
        getProperty(resource, property, statement -> {
            RDFNode obj = statement.getObject();
            if(obj.isLiteral()) {
               result.put("result", statement.getString());
            }
            if(obj.isResource()) {
                if(obj.asResource().listProperties().hasNext()) {
                    JsonObject nestedProperty = getNestedProperties(obj.asResource(), new HashMap<Property, String>() {
                        {
                            put(RDFS.label, "label");
                        }
                    });
                    if(!nestedProperty.isEmpty()) {
                        result.put("result", nestedProperty.getString("label"));
                    }
                } else {
                    result.put("result", obj.toString());
                }
            }
        });
        if(!result.isEmpty()) {
            return result.getString("result");
        } else {
            return null;
        }
    }

    @Deprecated
    public void getDistFormat(Resource resource, Handler<JsonObject> successHandler) {
        String format = determineSingeLiteral(resource, DCATAP.dctFormat);
        if(format != null) {
            successHandler.handle(new JsonObject()
                .put("id", JenaUtils.normalize(format))
                .put("title", format));
        }
    }

    @Deprecated
    public String resolveLanguage(String uri) {
        Resource resource = this.euVoc.getResource(uri);
        StmtIterator stms = resource.listProperties(EUVOC.atOpMappedCode);
        while (stms.hasNext()) {
            RDFNode rdfNode = stms.nextStatement().getObject();
            String h = rdfNode.asResource().getProperty(EUVOC.dcSource).getString();
            if(h.equals("iso-639-1")) {
                return rdfNode.asResource().getProperty(EUVOC.atLegacyCode).getString();
            }
        }
        return null;
    }






}
