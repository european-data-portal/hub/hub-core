package io.piveau.hub.util;

import io.piveau.utils.JenaUtils;
import io.vertx.core.json.JsonObject;

public class DCATAPUriSchema {

    private static String baseUri = "https://io.piveau/";
    private static String datasetContext = "set/data/";
    private static String distributionContext = "set/distribution/";
    private static String recordContext = "set/record/";
    private static String catalogueContext = "id/catalogue/";
    private static String validationContext = "id/validation/";

    private static String datasetGraphContext = datasetContext;
    private static String catalogueGraphContext = catalogueContext;
    private static String validationGraphContext = validationContext;

    private String id;

    public static void config(JsonObject config) {
        baseUri = config.getString("baseUri", baseUri);
        datasetContext = config.getString("datasetContext", datasetContext);
        distributionContext = config.getString("distributionContext", distributionContext);
        recordContext = config.getString("recordContext", recordContext);
        catalogueContext = config.getString("catalogueContext", catalogueContext);
        validationContext = config.getString("validationContext", validationContext);
        datasetGraphContext = config.getString("datasetGraphContext", datasetContext);
        catalogueGraphContext = config.getString("catalogueGraphContext", catalogueContext);
        validationGraphContext = config.getString("validationGraphContext", validationContext);
    }

    public static DCATAPUriSchema applyFor(String id) {
        return new DCATAPUriSchema(JenaUtils.normalize(id));
    }

    public static DCATAPUriSchema parseUriRef(String uriRef) {
        if (uriRef.startsWith(baseUri)) {
            String normalized = uriRef.substring(uriRef.lastIndexOf("/") + 1);
            return new DCATAPUriSchema(normalized);
        } else {
            return null;
        }
    }

    private DCATAPUriSchema(String id) {
        this.id = id;
    }

    public String id() {
        return id;
    }

    public String datasetGraphName() {
        return baseUri + datasetGraphContext + id;
    }

    public String catalogueGraphName() {
        return baseUri + catalogueGraphContext + id;
    }

    public String validationGraphName() {
        return baseUri + validationGraphContext + id;
    }

    public String datasetUriRef() {
        return baseUri + datasetContext + id;
    }

    public String distributionUriRef() {
        return baseUri + distributionContext + id;
    }

    public String recordUriRef() {
        return baseUri + recordContext + id;
    }

    public String catalogueUriRef() {
        return baseUri + catalogueContext + id;
    }

    public String validationUriRef() {
        return baseUri + validationContext + id;
    }

}
