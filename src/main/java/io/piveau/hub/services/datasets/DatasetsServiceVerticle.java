package io.piveau.hub.services.datasets;

import io.piveau.hub.util.Constants;
import io.piveau.hub.util.DataUploadConnector;
import io.piveau.hub.util.TSConnector;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.serviceproxy.ServiceBinder;

public class DatasetsServiceVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        WebClient client = WebClient.create(vertx);

        JsonObject conf = config().getJsonObject(Constants.ENV_HUB_TRIPLESTORE_CONFIG);
        JsonObject dataUploadconf = config().getJsonObject(Constants.ENV_DATA_UPLOAD);

        CircuitBreaker breaker = CircuitBreaker.create("virtuoso-breaker", vertx, new CircuitBreakerOptions().setMaxRetries(5))
                .retryPolicy(count -> count * 1000L);

        TSConnector connector = TSConnector.create(client, breaker, conf);
        DataUploadConnector dataUploadConnector = DataUploadConnector.create(client, dataUploadconf);

        DatasetsService.create(connector, dataUploadConnector, config(), vertx, ready -> {
            if (ready.succeeded()) {
                new ServiceBinder(vertx).setAddress(DatasetsService.SERVICE_ADDRESS).register(DatasetsService.class, ready.result());
                startFuture.complete();
            } else {
                startFuture.fail(ready.cause());
            }
        });
    }

}
