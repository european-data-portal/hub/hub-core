package io.piveau.hub.util.rdf;

import io.piveau.hub.util.Constants;
import io.piveau.hub.util.logger.PiveauLogger;
import io.piveau.hub.util.logger.PiveauLoggerFactory;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.json.JsonObject;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import java.io.ByteArrayInputStream;

public class VocabularyManager {


    public static Model model = ModelFactory.createDefaultModel();

    public static void init(JsonObject config, Vertx vertx, Handler<AsyncResult<Void>> handler) {
        if (config.getBoolean(Constants.ENV_HUB_LOAD_VOCABULARY, true)) {
            vertx.<Void>executeBlocking(fut -> loadVocabularies(vertx, fut.completer()), res -> {
                if (res.succeeded()) {
                    handler.handle(Future.succeededFuture());
                } else {
                    handler.handle(Future.failedFuture(res.cause()));
                }
            });
        } else {
            handler.handle(Future.succeededFuture());
        }
    }

    private static void loadVocabularies(Vertx vertx, Handler<AsyncResult<Void>> handler) {
        loadVocabulary(vertx, "vocabularies/languages-skos.rdf");
        loadVocabulary(vertx, "vocabularies/data-theme-skos-ap-act.rdf");
        loadVocabulary(vertx, "vocabularies/filetypes-skos.rdf");
        loadVocabulary(vertx, "vocabularies/countries-skos-ap-act.rdf");
        loadVocabulary(vertx, "vocabularies/continents-skos-ap-act.rdf");
        loadVocabulary(vertx, "vocabularies/licences-skos-edp.rdf");
        loadVocabulary(vertx, "vocabularies/corporatebodies-skos-ap-act.rdf");
        handler.handle(Future.succeededFuture());
    }

    private static void loadVocabulary(Vertx vertx, String file) {
        PiveauLogger LOGGER = PiveauLoggerFactory.getLogger(VocabularyManager.class);
        LOGGER.info("Init " + file);
        FileSystem fileSystem = vertx.fileSystem();
        Buffer buffer = fileSystem.readFileBlocking(file);
        RDFDataMgr.read(model, new ByteArrayInputStream(buffer.getBytes()), Lang.RDFXML);
        LOGGER.info("Created " + file + " Model");
    }
}
