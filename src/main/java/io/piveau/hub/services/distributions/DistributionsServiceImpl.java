package io.piveau.hub.services.distributions;

import io.piveau.hub.util.DCATAPUriSchema;
import io.piveau.hub.util.TSConnector;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DistributionsServiceImpl implements DistributionsService {


    private TSConnector connector;
    private JsonObject config;
    private Vertx vertx;

    DistributionsServiceImpl(TSConnector connector, JsonObject config, Vertx vertx, Handler<AsyncResult<DistributionsService>> readyHandler) {
        this.vertx = vertx;
        this.connector = connector;
        this.config = config;
        readyHandler.handle(Future.succeededFuture(this));
    }


    @Override
    public DistributionsService getDistribution(String id, String consumes, Handler<AsyncResult<JsonObject>> handler) {

        String graphName = DCATAPUriSchema.applyFor(id).distributionUriRef();

        connector.getDistribution(graphName, consumes, ar -> {
            if (ar.succeeded()) {
                //TODO: this will atm also return success, when the distribution is not found. Check for empty string will not work bc there will be some text in every representation. no representation has the same text.

                handler.handle(Future.succeededFuture(new JsonObject()
                        .put("status", "success")
                        .put("content", ar.result())
                ));
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
        return this;
    }

    @Override
    @Deprecated
    public DistributionsService postDistribution(String id, String dataset, String contentType, String catalogueId, String hash, Handler<AsyncResult<JsonObject>> handler) {
        handler.handle(Future.failedFuture("Not yet implemented"));
        return null;
    }

    @Override
    @Deprecated
    public DistributionsService deleteDistribution(String id, Handler<AsyncResult<JsonObject>> handler) {
        handler.handle(Future.failedFuture("Not yet implemented"));
        return null;
    }
}
